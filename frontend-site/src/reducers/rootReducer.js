import {candidateReducer} from "../components/Job/candidateReducer";
import {corporateCapstoneReducer} from "../components/Job/Corporate/Projects/corporateCapstoneReducer";
import {combineReducers} from "redux";
import reduceReducers from "reduce-reducers";

const rootReducer = reduceReducers(
    combineReducers({
        CANDIDATE: candidateReducer,
        CORPORATE_CAPSTONE: corporateCapstoneReducer,
        
    }),
);

export default rootReducer;
