import {CANDIDATE} from "../components/Job/initialState";
import {CORPORATE_CAPSTONE} from "../components/Job/Corporate/Projects/initialState";

const initalState = {
    CANDIDATE: CANDIDATE,
    CORPORATE_CAPSTONE: CORPORATE_CAPSTONE,
};

export default initalState;
