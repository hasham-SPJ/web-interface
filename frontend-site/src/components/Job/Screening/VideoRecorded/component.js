import React, {Component} from 'react';
import "./style.css";
import { Button }  from '@material-ui/core';
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
class VideoRecordedScore extends Component {

    render() {
        const {
            submitHandler,
        }  = this.props;
        return (
            <>
            <div className="jobaitestscorewrapper">
        <h1><CheckCircleOutlineIcon /> {"Successfully Recorded"}
        </h1>
                    <Button onClick={submitHandler} variant="contained" color="primary" >
                        Next
                    </Button>
            </div>
            </>
        )
    }
}

export default VideoRecordedScore;