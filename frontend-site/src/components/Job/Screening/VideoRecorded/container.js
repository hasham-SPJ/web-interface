import { connect } from "react-redux";
import VideoRecordedScore from "./component";

const mapStateToProps = state => ({
    screeningTest: state.CANDIDATE.data,
})

const mapDispatchToProps = (dispatch, props) => ({
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(VideoRecordedScore);