import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import VideoRecorder from 'react-video-recorder'

class CandidateScreeningReadingVideo extends Component {
    render() {
        const {
            submitHandler
        } = this.props;

        return (
            <div className="videorecordinggwrapper">
                <Grid container>
                    <Grid item sm={12} md={8} className="readingvideo">
                        <VideoRecorder 
                        timeLimit={10000}
                        onRecordingComplete={(videoBlob) => {
                        // Do something with the video...
                        console.log('videoBlob', videoBlob)
                        }} 
                    /> 
                    </Grid>

                    <Grid item sm={12} md={4}>
                        <div className="screeningreadingparagraph">
                            <h1>Please read the passage carefully.</h1>
                            <p>Carly's Family Carly has a large family. She lives with four people. Carly also has two pets. Carly’s mom is a doctor. Carly’s mom works at the hospital. Carly’s mom helps people who are sick. Carly’s dad works at home. Carly’s dad cooks for the family. Carly’s dad drives the kids to soccer practice.</p>
                        </div>
                        <Button variant="contained" color="primary" className="starttestbtn"
                            onClick={submitHandler}
                        >
                            Next
                        </Button>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default CandidateScreeningReadingVideo;