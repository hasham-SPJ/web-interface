import React, { Component } from "react";
import "./style.css";
import Button from '@material-ui/core/Button';
import QueryBuilderIcon from "@material-ui/icons/QueryBuilder";
import DesktopMacicon from "@material-ui/icons/DesktopMac";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CandidateScreeningVideo from "./VideoRecording/container";
import VideoRecordedScore from "./VideoRecorded/container";
import CandidateScreeningReadingVideo from "./ReadingVideo/container";
import CandidateScreeningListeningVideo from "./ListeningVideo/container";

class CandidateScreening extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAgree: false,
            isReadingAgree: false,
            isListeningAgree: false,
        }
    }
    handleChange = () => {
        this.setState({
            isAgree: !this.state.isAgree
        })
    }
    readingHandleChange = () => {
        this.setState({
            isReadingAgree: !this.state.isReadingAgree
        })
    }
    listeningHandleChange = () => {
        this.setState({
            isListeningAgree: !this.state.isListeningAgree
        })
    }
    render() {
        const {
            screeningTest,
            screeningTestStatusHandler,
            screeningTestStatusCompletedHandler,
            screeningState,
            updateStateToSecond,
            updateStateToThird,
            screeningReadingStatusHandler,
            screeningReadingCompletedStatusHandler,
            screeningListeningStatusHandler,
            screeningListeningCompletedStatusHandler
        } = this.props;

        return (
            <>
            {
                screeningState === 1 ? 
            <div className="screeningwrapper">

                {screeningTest.test.status === "pending" ? 
                <>
                    <h3>Screening/Introduction Video</h3>
                    <div className="titlewithicon">
                        <QueryBuilderIcon />
                        <span>Time Duration: 10:00</span>
                    </div>
                    <p>

                    <div className="titlewithicon">
                        <DesktopMacicon />
                        <span>Instructions</span>
                    </div> <br></br>

                    Introduce yourself in 4-5 lines.<br></br>
                    After Clicking "Start Screening" your timer will start.<br></br>
                    Timer will be of 2 minutes. <br></br>
                    There will be no retake of this test. 
                    
                    </p>

                    <p className="screeningcheckbox">
                        <FormControlLabel
                            control={
                            <Checkbox
                                checked={this.state.isAgree}
                                onChange={this.handleChange}
                                name="checkedB"
                                color="primary"
                            />
                            }
                        />
                        Yes, I carefully read the instructions above ready to go in.</p>

                        <div className="screeningbgetstart">
                            <h1>Click "Start Screening" to get started</h1>
                            <div className="startscreeningbbtn">
                                <Button variant="contained" disabled={this.state.isAgree ? false: true} color="primary" className="starttestbtn"
                                    onClick={screeningTestStatusHandler}
                                >
                                    Start Screening
                                </Button>
                            </div>
                        </div>
                </>
                : null }
                {
                    screeningTest.test.status === "inprogress" ? 
                    // <TestTemplate testDetail={screeningTest} submitHandler={screeningTestStatusCompletedHandler} /> 
                    <CandidateScreeningVideo submitHandler={screeningTestStatusCompletedHandler}/>
                    : null
                }

                {
                    screeningTest.test.status === "completed" ? 
                    <>
                        <VideoRecordedScore submitHandler={updateStateToSecond} score={screeningTest.test.score}/>
                    </>
                    : null
                }
            </div> : null }


            {
                screeningState === 2 ? 
            <div className="screeningwrapper">

                {screeningTest.test.readingStatus === "pending" ? 
                <>
                    <h3>Screening/Paragraph Reading</h3>
                    <div className="titlewithicon">
                        <QueryBuilderIcon />
                        <span>Time Duration: 10:00</span>
                    </div>
                    <p>

                    <div className="titlewithicon">
                        <DesktopMacicon />
                        <span>Instructions</span>
                    </div> <br></br>

                    Please read the passage carefully given in the screen <br></br>
                    After Clicking "Start Screening" your timer will start.<br></br>
                    Timer will be of 2 minutes. <br></br>
                    There will be no retake of this test.
                    
                    </p>

                    <p className="screeningcheckbox">
                        <FormControlLabel
                            control={
                            <Checkbox
                                checked={this.state.isReadingAgree}
                                onChange={this.readingHandleChange}
                                name="checkedB"
                                color="primary"
                            />
                            }
                        />
                        Yes, I carefully read the instructions above ready to go in.</p>

                        <div className="screeningbgetstart">
                            <h1>Click "Start Screening" to get started</h1>
                            <div className="startscreeningbbtn">
                                <Button variant="contained" disabled={this.state.isReadingAgree ? false: true} color="primary" className="starttestbtn"
                                    onClick={screeningReadingStatusHandler}
                                >
                                    Start Screening
                                </Button>
                            </div>
                        </div>
                </>
                : null }
                {
                    screeningTest.test.readingStatus === "inprogress" ? 
                    // <TestTemplate testDetail={screeningTest} submitHandler={screeningTestStatusCompletedHandler} /> 
                    <CandidateScreeningReadingVideo submitHandler={screeningReadingCompletedStatusHandler}/>
                    : null
                }

                {
                    screeningTest.test.readingStatus === "completed" ? 
                    <>
                        <VideoRecordedScore submitHandler={updateStateToThird} score={screeningTest.test.score}/>
                    </>
                    : null
                }
            </div> : null }


            {
                screeningState === 3 ? 
            <div className="screeningwrapper">

                {screeningTest.test.englishStatus === "pending" ? 
                <>
                    <h3>Screening/Paragraph Reading</h3>
                    <div className="titlewithicon">
                        <QueryBuilderIcon />
                        <span>Time Duration: 10:00</span>
                    </div>
                    <p>

                    <div className="titlewithicon">
                        <DesktopMacicon />
                        <span>Instructions</span>
                    </div> <br></br>

                    Please listen to the narrated story carefully.<br></br>
                    Listen to the questions by playing the questions audio player.<br></br>
                    Then answer the questions.<br></br>
                    To do this on computer you need to have Microphone connected to your computer.<br></br>
                    After Clicking "Start Screening" your timer will start.<br></br>
                    Timer will be of 2 minutes<br></br>
                    There will be no retakes of the test
                    </p>

                    <p className="screeningcheckbox">
                        <FormControlLabel
                            control={
                            <Checkbox
                                checked={this.state.isListeningAgree}
                                onChange={this.listeningHandleChange}
                                name="checkedB"
                                color="primary"
                            />
                            }
                        />
                        Yes, I carefully read the instructions above ready to go in.</p>

                        <div className="screeningbgetstart">
                            <h1>Click "Start Screening" to get started</h1>
                            <div className="startscreeningbbtn">
                                <Button variant="contained" disabled={this.state.isListeningAgree ? false: true} color="primary" className="starttestbtn"
                                    onClick={screeningListeningStatusHandler}
                                >
                                    Start Screening
                                </Button>
                            </div>
                        </div>
                </>
                : null }
                {
                    screeningTest.test.englishStatus === "inprogress" ? 
                    // <TestTemplate testDetail={screeningTest} submitHandler={screeningTestStatusCompletedHandler} /> 
                    <CandidateScreeningListeningVideo submitHandler={screeningListeningCompletedStatusHandler}/>
                    : null
                }

                {
                    screeningTest.test.englishStatus === "completed" ? 
                    <>
                        <VideoRecordedScore isListening={true} score={screeningTest.test.score}/>
                    </>
                    : null
                }
            </div> : null }

            </>
        )
    }
}

export default CandidateScreening;