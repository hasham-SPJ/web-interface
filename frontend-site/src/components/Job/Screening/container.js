import { connect } from "react-redux";
import CandidateScreening from "./component";
import {
    screeningTestStatusHandler,
    updateScreeningCurrentQuestionCounter,
    screeningStateHandler,
    screeningReadingStatusHandler,
    screeningListeningStatusHandler
} from "../actions";

const mapStateToProps = state => ({
    screeningTest: state.CANDIDATE.data.tests.screening,
    screeningState: state.CANDIDATE.data.tests.screening.test.state

})

const mapDispatchToProps = (dispatch, props) => ({
    screeningTestStatusHandler: () => {
        dispatch(screeningTestStatusHandler("inprogress"));
    },
    screeningTestStatusCompletedHandler: () => {
        dispatch(screeningTestStatusHandler("completed"));
    },
    screeningReadingStatusHandler: () => {
        dispatch(screeningReadingStatusHandler("inprogress"));
    },
    screeningReadingCompletedStatusHandler: () => {
        dispatch(screeningReadingStatusHandler("completed"));
    },
    screeningListeningStatusHandler: (value) => {
        dispatch(screeningListeningStatusHandler("inprogress"));
    },
    screeningListeningCompletedStatusHandler: (value) => {
        dispatch(screeningListeningStatusHandler("completed"));
    },
    updateScreeningCurrentQuestionCounter: (value) => {
        dispatch(updateScreeningCurrentQuestionCounter(value + 1));
    },
    updateStateToSecond: () => {
        dispatch(screeningStateHandler(2));
    },
    updateStateToThird: () => {
        dispatch(screeningStateHandler(3));
    }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    updateScreeningCurrentQuestionCounter: () => {
        dispatchProps.updateScreeningCurrentQuestionCounter(stateProps.screeningTest.test.currentQuestion);
    },
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(CandidateScreening);