import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import VideoRecorder from 'react-video-recorder'
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import ReactAudioPlayer from 'react-audio-player';
import SoundFile from "../../../../sounds/1.mp3";

class CandidateScreeningListeningVideo extends Component {
    render() {
        const {
            submitHandler
        } = this.props;

        return (
            <div className="videorecordinggwrapper">
                <Grid container>
                <Grid item sm={12} md={4}>
                        <div className="screeninglisteningparagraph">
                            <h1>Please read all the guidelines below.</h1>
                            <p>
                            Keep your Camera and Microphone active to avoid any hassle. <br></br>
                            Press <RadioButtonCheckedIcon className="stop" /> button to start recording. <br></br>
                            Press <PlayArrowIcon className="start" />button to play narrated phrase. <br></br>
                            Questions will be asked related to narrated phrase or story. <br></br>
                            Try to answer all of the question in given time interval. <br></br>
                            After audio is finished recording will be automatically stop.
                            </p>

                            <ReactAudioPlayer
                                src={SoundFile}
                                controls={true}
                            />
                        </div>
                        
                    </Grid>
                    <Grid item sm={12} md={8} className="readingvideo">
                        <VideoRecorder 
                        timeLimit={10000}
                        onRecordingComplete={(videoBlob) => {
                        // Do something with the video...
                        console.log('videoBlob', videoBlob)
                        }} 
                    /> 
                    <Button variant="contained" color="primary" className="starttestbtn"
                            onClick={submitHandler}
                        >
                        Next
                    </Button>
                    </Grid>

                    
                </Grid>
            </div>
        )
    }
}

export default CandidateScreeningListeningVideo;