import React, {Component} from 'react';
import "./style.css";
import { Button, Typography, Grid }  from '@material-ui/core';
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
class TestScore extends Component {

    render() {
        const {
            score,
            nextSubmitHandler,
            showButton,
            isVideo
        }  = this.props;
        return (
            <>
            <div className="jobaitestscorewrapper">
        <h1><CheckCircleOutlineIcon /> {
            isVideo ? "Successfully Recorded" : 
        "Test Submitted Successfully" }
        </h1>
        {isVideo ? null : <h3>Score <span>{score}</span></h3> }
                    {
                        showButton ? 
                    <Button onClick={nextSubmitHandler} variant="contained" color="primary" >
                        Next
                    </Button> : null }
            </div>
            </>
        )
    }
}

export default TestScore;