import { connect } from "react-redux";
import EITTestBox from "./component";

const mapStateToProps = state => ({
    eittest: state.CANDIDATE.data.tests.eittest,
})

const mapDispatchToProps = (dispatch, props) => ({
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(EITTestBox);