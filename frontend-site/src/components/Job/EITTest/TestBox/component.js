import React, {Component} from 'react';
import "./style.css";
import { Button, Typography, Grid }  from '@material-ui/core';
import EitTestImg from "../../../../eittestpic.png"
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Timer from "react-compound-timer";
import QueryBuilderIcon from "@material-ui/icons/QueryBuilder";
class EITTestBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentId: 0,
        }
    }

    changeQuestionHandler = (value) => {
        this.setState({
            currentId: value
        })
    }
    render() {
        const {
            eittest,
        } = this.props;

        const currenQuestion = eittest.test.questions;
        return (
            <>
            
            <div className="eittestboxpage">
                {
                    currenQuestion.length ? 
            <>
            <Grid container>
                <Grid item sm={12} className="eittestboxtitle">
                    <h2 >JOBAi/<span>Candidate Flow/Emotional Intelligence Test</span></h2>
                <Timer
                        initialTime={600000}
                        direction="backward"
                    >
                        {() => (
                            <React.Fragment>
                                <h2 class="timer"> 
                                     <QueryBuilderIcon />
                                     Time Left: 
                                    <Timer.Minutes /> <span>: </span>
                                    <Timer.Seconds /> <span></span>
                                </h2>
                            </React.Fragment>
                        )}
                    </Timer>
                </Grid>
            </Grid>
                <Grid container>
                    <Grid item xs={12} sm={12} md={12}>
                    <div className="eittestboxwrapper"> 
                        <h1>How Good You are At <span>"Emotional Intelligence"</span></h1>
                        <Grid container>
                            <Grid item sm={12} md={4}>
                                <img src={`data:image/png;base64,${currenQuestion[this.state.currentId].image}`}></img>
                                <div className="eittestpagination">

                                    {
                                        currenQuestion.map((question , index) => (
                                            <Button onClick={(e) => this.changeQuestionHandler(index)} variant="contained" color="primary">
                                                    {index}
                                            </Button>
                                        ))
                                    }
                                    
                                </div>
                            </Grid>
                            <Grid item sm={12} md={8}>
                                <div className="eittestcontent">
                                    <h3>{currenQuestion[this.state.currentId].content}</h3>

                                    <FormControl component="fieldset" className="eitradiogroup">
                                        <RadioGroup aria-label="gender" name="gender1" >
                                            <FormControlLabel value="Anger" control={<Radio />} label="Anger"
                                            //  onClick={(e) => saveRightAnswer(currentQuestion.rightAnswer, test.answer, testDetail.test.score)}
                                            />
                                            <FormControlLabel value="Disgust" control={<Radio />} label="Disgust" />
                                            <FormControlLabel value="Fear" control={<Radio />} label="Fear" />
                                            <FormControlLabel value="Embarassment" control={<Radio />} label="Embarrasment" />
                                        </RadioGroup>
                                    </FormControl> 
                                    
                                </div>
                            </Grid>
                            <Grid item md={12}>
                                <Button   href="/" variant="contained" color="primary">
                                    Submit
                                </Button> 
                            </Grid>
                        </Grid>                                         
                    </div>
                    </Grid>
                </Grid>

                </> : "Loading Data" }
            </div>
            </>
        )
    }
}

export default EITTestBox;