import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import JobAICorporateHeader from "./Header/container";
import AssignmentIcon from "@material-ui/icons/Assignment";
import PageViewIcon from "@material-ui/icons/Pageview";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import EmojiEventsIcon from "@material-ui/icons/EmojiEvents";
import CorporateDashoardImg from "../../../../jobaicorporate.png";

class JobAICorporateDashboard extends Component {

    render() {
        return (
            <>
                <JobAICorporateHeader />
                <div className="jobaicorporatedashboard">
                    <Grid container justify="center">
                        <Grid item sm={10}>
                            <h2 className="title" >What would you like to do?</h2>
                        </Grid>
                    </Grid>
                    <Grid container justify="center">
                        <Grid item sm={10}>
                            <Grid container justify="center">
                                <Grid item xs={12} sm={12} md={12} lg={8} >
                                    <div className="jobaicorporatedashboardoptions">
                                        <a href="/jobai/corporate-assign-project"><AssignmentIcon /> Assign Project</a>
                                        <a href="/jobai/corporate-view-project"><PageViewIcon />View Project</a>
                                        <a href="/jobai/corporate-evaluated-project"><AssignmentTurnedInIcon />Pre-Evaluated</a>
                                        <a href="/jobai/corporate-passed-project"><EmojiEventsIcon />Passed</a>
                                    </div>
                                </Grid>
                                <Grid item xs={12} sm={12} md={12} lg={4} >
                                    <img src={CorporateDashoardImg} ></img>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            </>
        )
    }
}

export default JobAICorporateDashboard;