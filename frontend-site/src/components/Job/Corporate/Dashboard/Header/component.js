import React, { Component } from "react";
import "./style.css";
import { Grid } from '@material-ui/core';


class JobAICorporateHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 1,
        }
    }

    submitHandler = (value) => {
        // this.setState({
        //     id: value,
        // })
    }
    render() {
        return (
            <div className="jobaicorporateheader">
                <Grid container>
                    <Grid item sm={12} md={4}>
                        <div className="headertitle">
                            <ul>
                                <li><h1>JobAi</h1></li>
                                <li><span>Reviewer Name</span></li>
                            </ul>
                        </div>
                    </Grid>
                    <Grid item sm={12} md={8}>
                        <div className="headertitleright">
                            <ul>
                                <li><a onClick={(e) => this.submitHandler(0) } className={this.state.id === 0 ? "active" : null} href="/jobai/corporate/">Dashboard</a></li>
                                <li><a onClick={(e) => this.submitHandler(1) } className={this.state.id === 1 ? "active" : null}  href="/jobai/corporate-assign-project">Capstone Project</a></li>
                                <li><a onClick={(e) => this.submitHandler(2) } className={this.state.id === 2 ? "active" : null} href="#">Feature X</a></li>
                                <li><a onClick={(e) => this.submitHandler(3) } className={this.state.id === 3 ? "active" : null}  href="#">Feature Y</a></li>
                                <li><a onClick={(e) => this.submitHandler(4) } className={this.state.id === 4 ? "active" : null}  href="#">Feature Z</a></li>
                            </ul>
                        </div>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default JobAICorporateHeader;