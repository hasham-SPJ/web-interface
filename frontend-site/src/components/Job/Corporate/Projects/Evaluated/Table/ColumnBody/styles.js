export const styles = theme => ({
	flexGrow: {
		flex: 1,
		textAlign: "left"
	},
	active:{
		color: "#0080FF",
		textAlign: "center",
		display: "block",
		fontWeight: "700"
	},
	inactive:{
		color: "#F83232",
		textAlign: "center",
		display: "block",
	},
	normal:{
		color: "#303030",
		textAlign: "center",
		display: "block",
	},
	corporatereviewertableoptions: {
		textAlign: "center",
		"& .MuiFormControl-root": {
			width: "81%",
			border: "1px solid #E0E0E0",
			background: "white",
			borderRadius: "3px",
		}
	},
	coporaterevaluatededitsbtn: {
		margin: "auto",
		display: "block",
		marginTop: "1px !important",
		background: "none",
		boxShadow: "none !important",
		color: "black",
		fontWeight: "800",
		"&:hover": {
			background: "none",
		}
	},
	coporaterevaluatededitsbtnblue: {
		margin: "auto",
		display: "block",
		marginTop: "1px !important",
		background: "none",
		boxShadow: "none !important",
		color: "#0080FF",
		fontWeight: "800",
		textTransform: "capitalize",
		"&:hover": {
			background: "none",
		}
	}
});
