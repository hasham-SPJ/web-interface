import React, { Component } from "react";
import { styles } from "./styles";
import { withStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import EditIcon from "@material-ui/icons/Edit";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

class ColumnBody extends Component {
	componentDidMount() {}

	componentDidUpdate() {}

	editPersonHandler = e => {
		// this.props.editPersonHandler(this.props.cellProps.tableMeta.rowIndex);
	};

	deletePerson = e => {
		// this.props.deletePersonHandler(this.props.cellProps.tableMeta.rowIndex);
	};

	render() {
		const { editPersonHandler, classes, cellProps, openViewDialog, ...rest } = this.props;

		return cellProps.tableMeta.columnData.name === "status" ? (
			<>
				<FormControl  className={classes.formControl}>
						{/* <InputLabel id="demo-simple-select-outlined-label">Select</InputLabel> */}
						<Select
						// labelId="demo-simple-select-outlined-label"
						// id="demo-simple-select-outlined"
						value="Select"
						// onChange={handleChange}
						// label="Select"
						>
						<MenuItem selected={true} value="Select">
							{/* {cellProps.tableMeta.rowData[2]} */}
							Select
						</MenuItem>
						<MenuItem value="Pass">Pass</MenuItem>
						<MenuItem value="Fail">Fail</MenuItem>
						</Select>
					</FormControl>
			</>
		): (
			
			(cellProps.tableMeta.columnData.name === "email" || cellProps.tableMeta.columnData.name === "meetinglink") ? 
						<span className={classes.active}>{cellProps.value}</span> :
				(
					cellProps.tableMeta.columnData.name === "edit" ? 
					<Button variant="contained" color="primary" className={classes.coporaterevaluatededitsbtn} >
						<EditIcon />
					</Button> : 
					<span className={classes.normal}>{cellProps.value}</span>
				)
			
		)
	}
}

ColumnBody.propTypes = {};

export default withStyles(styles)(ColumnBody);
