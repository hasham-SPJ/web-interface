import { connect } from "react-redux";
import JobAICorporateEvaluatedProject from "./component";
import instance, { CAPSTONE_BASE_URL } from "../../../../../api/config";
import {
    getPreEvaluatedProjectsTableData,
    isLoadingStateHandler
} from "../actions"

const mapStateToProps = state => ({
    evaluatedProjectsData: state.CORPORATE_CAPSTONE.listData.Capstone.Corporate.evaluatedProjects,
    isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
})

const mapDispatchToProps = (dispatch, props) => ({
    fetchDataIfNeeded: () => {
        dispatch(isLoadingStateHandler(true));
        instance.get(CAPSTONE_BASE_URL+'Scheduled')
        .then(res => {
            dispatch(isLoadingStateHandler(false));
            dispatch(getPreEvaluatedProjectsTableData(res.data));
        })
        .catch(error => {
            dispatch(isLoadingStateHandler(false));
        })
      }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(JobAICorporateEvaluatedProject);