import  {
    CORPORATE_CAPSTONE_GET_ASSIGN_NEW_DATA,
    CORPORATE_CAPSTONE_IS_LOADING_HANDLER,
    CORPORATE_CAPSTONE_GET_PENDING_NEW_DATA,
    CORPORATE_CAPSTONE_GET_ON_GOING_NEW_DATA,
    CORPORATE_CAPSTONE_GET_SUBMITTED_PROJECTS_NEW_DATA,
    CORPORATE_CAPSTONE_GET_PRE_EVALUATED_PROJECTS_NEW_DATA,
    CORPORATE_CAPSTONE_GET_PASSED_PROJECTS_NEW_DATA
} from "./actions";

export const corporateCapstoneReducer = (state = [], action) => {
    switch (action.type) {
        case CORPORATE_CAPSTONE_GET_ASSIGN_NEW_DATA: {
            return {
                ...state,
                listData: {
                    ...state.listData,
                    Capstone: {
                        ...state.listData.Capstone,
                        Corporate: {
                            ...state.listData.Capstone.Corporate,
                            assignNew: action.payload
                        }
                    }
                }
            }
        }

        case CORPORATE_CAPSTONE_GET_PENDING_NEW_DATA: {
            return {
                ...state,
                listData: {
                    ...state.listData,
                    Capstone: {
                        ...state.listData.Capstone,
                        Corporate: {
                            ...state.listData.Capstone.Corporate,
                            pending: action.payload
                        }
                    }
                }
            }
        }

        case CORPORATE_CAPSTONE_GET_ON_GOING_NEW_DATA: {
            return {
                ...state,
                listData: {
                    ...state.listData,
                    Capstone: {
                        ...state.listData.Capstone,
                        Corporate: {
                            ...state.listData.Capstone.Corporate,
                            onGoing: action.payload
                        }
                    }
                }
            }
        }

        case CORPORATE_CAPSTONE_GET_SUBMITTED_PROJECTS_NEW_DATA: {
            return {
                ...state,
                listData: {
                    ...state.listData,
                    Capstone: {
                        ...state.listData.Capstone,
                        Corporate: {
                            ...state.listData.Capstone.Corporate,
                            submittedProjects: action.payload
                        }
                    }
                }
            }
        }

        case CORPORATE_CAPSTONE_GET_PRE_EVALUATED_PROJECTS_NEW_DATA: {
            return {
                ...state,
                listData: {
                    ...state.listData,
                    Capstone: {
                        ...state.listData.Capstone,
                        Corporate: {
                            ...state.listData.Capstone.Corporate,
                            evaluatedProjects: action.payload
                        }
                    }
                }
            }
        }

        case CORPORATE_CAPSTONE_GET_PASSED_PROJECTS_NEW_DATA: {
            return {
                ...state,
                listData: {
                    ...state.listData,
                    Capstone: {
                        ...state.listData.Capstone,
                        Corporate: {
                            ...state.listData.Capstone.Corporate,
                            passedProjects: action.payload
                        }
                    }
                }
            }
        }

        case CORPORATE_CAPSTONE_IS_LOADING_HANDLER: {
            return {
                ...state,
                UISetting: {
                    ...state.UISetting,
                    isLoading: action.payload
                }
            }
        }
        default: {
            return {
                ...state
            };
        }
    }
}