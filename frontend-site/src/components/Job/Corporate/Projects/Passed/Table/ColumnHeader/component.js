import React, {Component} from "react";
import {styles} from "./styles";
import {withStyles} from "@material-ui/core/styles";

class ColumnHeader extends Component {
    componentDidMount() {
    }

    componentDidUpdate() {
    }

    render() {
        const {classes, columnProps,colWidth} = this.props;
        return (
            <th 
            className={classes.columnheader}
            style={{width: colWidth || ''}}
            >
                <div className={classes.labelOnly}>
                    {
                        columnProps.props.name === "actions" ? '' : columnProps.props.label
                    }
                    </div>
            </th>
        );
    }
}

ColumnHeader.propTypes = {};

export default withStyles(styles)(ColumnHeader);
