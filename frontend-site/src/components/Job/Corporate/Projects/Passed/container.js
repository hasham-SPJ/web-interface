import { connect } from "react-redux";
import JobAICorporatePassedProject from "./component";
import instance, { CAPSTONE_BASE_URL } from "../../../../../api/config";
import {
    getPassedProjectsTableData,
    isLoadingStateHandler
} from "../actions"

const mapStateToProps = state => ({
    passedProjectsData: state.CORPORATE_CAPSTONE.listData.Capstone.Corporate.passedProjects,
    isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
})

const mapDispatchToProps = (dispatch, props) => ({
    fetchDataIfNeeded: () => {
        instance.get(CAPSTONE_BASE_URL+'Passed')
        .then(res => {
            dispatch(getPassedProjectsTableData(res.data));
        })
        .catch(error => {
            
        })
    }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(JobAICorporatePassedProject);