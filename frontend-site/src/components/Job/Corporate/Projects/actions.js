export const CORPORATE_CAPSTONE_GET_ASSIGN_NEW_DATA = "CORPORATE_CAPSTONE_GET_ASSIGN_NEW_DATA";
export const CORPORATE_CAPSTONE_GET_PENDING_NEW_DATA = "CORPORATE_CAPSTONE_GET_PENDING_NEW_DATA";
export const CORPORATE_CAPSTONE_GET_ON_GOING_NEW_DATA = "CORPORATE_CAPSTONE_GET_ON_GOING_NEW_DATA";
export const CORPORATE_CAPSTONE_GET_SUBMITTED_PROJECTS_NEW_DATA = "CORPORATE_CAPSTONE_GET_SUBMITTED_PROJECTS_NEW_DATA";
export const CORPORATE_CAPSTONE_GET_PRE_EVALUATED_PROJECTS_NEW_DATA = "CORPORATE_CAPSTONE_GET_PRE_EVALUATED_PROJECTS_NEW_DATA";
export const CORPORATE_CAPSTONE_GET_PASSED_PROJECTS_NEW_DATA = "CORPORATE_CAPSTONE_GET_PASSED_PROJECTS_NEW_DATA";
export const CORPORATE_CAPSTONE_IS_LOADING_HANDLER = "CORPORATE_CAPSTONE_IS_LOADING_HANDLER";

export const getAssignNewTableData = value => {
  let data = [];
  let test;
  value.map((val) => {
      test = Object.values(val)
      data = [...data, test]
  })
  return {
    type: CORPORATE_CAPSTONE_GET_ASSIGN_NEW_DATA,
    payload: data
  }
}

export const getPendingTableData = value => {
  let data = [];
  let test;
  value.map((val) => {
      test = Object.values(val)
      data = [...data, test]
  })
  return {
    type: CORPORATE_CAPSTONE_GET_PENDING_NEW_DATA,
    payload: data
  }
}

export const getOnGoingTableData = value => {
  let data = [];
  let test;
  value.map((val) => {
      test = Object.values(val)
      data = [...data, test]
  })
  return {
    type: CORPORATE_CAPSTONE_GET_ON_GOING_NEW_DATA,
    payload: data
  }
}

export const getSubmitedProjectsTableData = value => {
  let data = [];
  let test;
  value.map((val) => {
      test = Object.values(val)
      data = [...data, test]
  })
  return {
    type: CORPORATE_CAPSTONE_GET_SUBMITTED_PROJECTS_NEW_DATA,
    payload: data
  }
}

export const getPreEvaluatedProjectsTableData = (value) => {
  let data = [];
  let test;
  value.map((val) => {
      test = Object.values(val)
      data = [...data, test]
  })
  return {
    type: CORPORATE_CAPSTONE_GET_PRE_EVALUATED_PROJECTS_NEW_DATA,
    payload: data
  }
}

export const getPassedProjectsTableData = (value) => {
  let data = [];
  let test;
  value.map((val) => {
      test = Object.values(val)
      data = [...data, test]
  })
  return {
    type: CORPORATE_CAPSTONE_GET_PASSED_PROJECTS_NEW_DATA,
    payload: data
  }
}

export const isLoadingStateHandler = value => {
  return {
    type: CORPORATE_CAPSTONE_IS_LOADING_HANDLER,
    payload: value
  }
}