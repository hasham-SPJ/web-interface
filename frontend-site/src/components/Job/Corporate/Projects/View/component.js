import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import JobAICorporateHeader from "../../Dashboard/Header/container";
import MUIDataTable from "mui-datatables";
import ColumnHeader from "./Table/ColumnHeader/container";
import ColumnBody from "./Table/ColumnBody/container";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import JobAICorporateViewProjectDialog from "./Table/ViewDialog/container";

class JobAICorporateViewProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isView: false,
            viewDialogId: "",
        }
    }

    openViewDialog = (value) => {
        this.setState({
            isView: true,
            viewDialogId: value
        })
    }

    closeViewDialog = () => {
        this.setState({
            isView: false,
        })
    }

    componentDidMount() {
        this.props.fetchDataIfNeeded();
    }
    render() {

        const {
            submittedProjectsData
        } = this.props;
        
        const columns = [
            {
             name: "name",
             label: "Name",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                    colWidth="210px"
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "id",
             label: "ID",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "vertical",
             label: "Vertical",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "level",
             label: "Level",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "kickoff",
             label: "Kickoff",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
            }
            },
            {
             name: "deadline",
             label: "Deadline",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
            }
            },
            {
             name: "submission",
             label: "Submission",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                    customBodyRender: (value, tableMeta, updateValue ) => (
                        <ColumnBody 
                            cellProps ={{
                                value: value,
                                tableMeta: tableMeta,
                                updateValue: updateValue,
                            }}
                        />
                    )
            }
            },
            {
                name: "detail",
                label: "Detail",
                options: {
                   filter: true,
                   sort: false,
                   customHeadRender: (props) => (
                       <ColumnHeader 
                           columnProps ={{
                               props: {...props},
                           }}
                       />
                       ),
                       customBodyRender: (value, tableMeta, updateValue ) => (
                        <ColumnBody 
                            openViewDialog={this.openViewDialog}
                            cellProps ={{
                                value: value,
                                tableMeta: tableMeta,
                                updateValue: updateValue,
                            }}
                        />
                    )
               }
               },
           ];
        // const columns = ["Name", "ID", "Parameter X", "Parameter Y", "Vertical", "Level", "Status"];

        const data1 = [
        ["Joe James", "1", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["John Walsh", "2", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["Bob Herm", "3", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["James Houston", "4", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["Joe James", "1", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["John Walsh", "2", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["Bob Herm", "3", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["James Houston", "4", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["Joe James", "1", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["John Walsh", "2", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["Bob Herm", "3", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ["James Houston", "4", "Education", "Intermediate", "April 13, 2020" , "April 23, 2020", "April 20, 2020", ""],
        ];

        const data = submittedProjectsData

        const options = {
        filterType: 'checkbox',
        print: false,
        download: false,
        viewColumns: false,
        filter: false,
        selectableRowsHeader: false,
        };

        return (
            <>

                {
                    this.state.isView && (
                        <Dialog
                            open={this.state.isView}
                            onClose={this.closeViewDialog}
                            className="revieweropendialog"
                            maxWidth="md"
                            fullWidth={true}
                        >
                            {/* <DialogTitle>123</DialogTitle> */}
                            <DialogContent>
                                <JobAICorporateViewProjectDialog viewDialogId={this.state.viewDialogId} closeViewDialog={this.closeViewDialog}/>
                            </DialogContent>
                            <DialogActions>
                            
                            </DialogActions>
                        </Dialog>
                    )
                }
                <JobAICorporateHeader />
                <div className="jobaicorporateview">

                    <Grid container justify="center">
                        <Grid item sm={10} className="jobaicorporatetables">
                            <MUIDataTable
                            data={data} 
                            columns={columns} 
                            options={options}
                            /> 
                        </Grid>
                    </Grid>
                </div>
            </>
        )
    }
}

export default JobAICorporateViewProject;