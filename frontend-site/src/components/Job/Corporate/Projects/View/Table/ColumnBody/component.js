import React, { Component } from "react";
import { styles } from "./styles";
import { withStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';

class ColumnBody extends Component {
	componentDidMount() {}

	componentDidUpdate() {}

	editPersonHandler = e => {
		// this.props.editPersonHandler(this.props.cellProps.tableMeta.rowIndex);
	};

	deletePerson = e => {
		// this.props.deletePersonHandler(this.props.cellProps.tableMeta.rowIndex);
	};

	render() {
		const { editPersonHandler, classes, cellProps, openViewDialog, ...rest } = this.props;

		return cellProps.tableMeta.columnData.name === "kickoff" ? (
			<>
				<span className={classes.active}>{cellProps.value}</span>
			</>
		): (
			
			(cellProps.tableMeta.columnData.name === "deadline") ? 
				<span className={classes.inactive}>{cellProps.value}</span> :
				(
					cellProps.tableMeta.columnData.name === "detail" ? 
					<Button onClick={(e) => openViewDialog(cellProps.tableMeta.rowData[0])} variant="contained" color="primary" className={classes.coporatereviwerdialognbtn} >
						View
					</Button> : 
					<span className={classes.normal}>{cellProps.value}</span>
				)
			
		)
	}
}

ColumnBody.propTypes = {};

export default withStyles(styles)(ColumnBody);
