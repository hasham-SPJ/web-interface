export const styles = theme => ({
	flexGrow: {
		flex: 1,
		textAlign: "left"
	},
  labelOnly:{
		paddingTop:3,
		color: "#0080FF",
	},
	columnheader: {
		borderBottom: '1px solid #e0e0e0',
	}
});
