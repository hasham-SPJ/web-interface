import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import TimePicker from 'react-time-picker';
import GetAppIcon from "@material-ui/icons/GetApp";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';

class JobAICorporateViewProjectDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            time: '10:00',
        }
    }

    onChange = date => this.setState({ date })
    onChange = time => this.setState({ time })
    componentDidMount() {
        this.props.fetchDataIfNeeded();
    }
    render() {
        const  {
            closeViewDialog,
            viewDialogId,
            viewProjects
        } =  this.props;

        return (
            <>
                <div className="jobaicorporateviewDialogWrapper">

                    <Grid container justify="center">
                        <Grid item sm={12} className="jobaicorporatetables">
                        <Button className="closebtn" onClick={closeViewDialog} variant="contained" color="primary">
                                X
                            </Button>
                        </Grid>
                        <Grid item xs={12} sm={12} md={7} className="rightgridpadding" >
                            <div className="candidatedetaildialog">
                                <span className="title">Candidate</span>
                                <div className="nameid">
                                <h3>{viewDialogId}</h3>
                                    {/* <h4>ID: 123123</h4> */}
                                </div>
                                <span className="titles">Project</span>
                                <div className="project">
                                    <a href="#" download><GetAppIcon />Landing Page Development</a>
                                </div>
                                <div className="verticalrow">
                                    <div className="vertical">
                                        <span className="titles">Vertical</span>
                                        <h3>Education</h3>
                                    </div>
                                    <div className="vertical">
                                        <span className="titles">Level</span>
                                        <h3>Intermediate</h3>
                                    </div>
                                </div>
                                <div className="verticalrow">
                                    <div className="vertical">
                                        <span className="titles">Kickoff</span>
                                        <h3 className="greendate" >April 13, 2020</h3>
                                    </div>
                                    <div className="vertical">
                                        <span className="titles">Dedline</span>
                                        <h3 className="orangedate" >April 23, 2020</h3>
                                    </div>
                                    <div className="vertical">
                                        <span className="titles">Submission</span>
                                        <h3>April 20, 2020</h3>
                                    </div>
                                </div>
                                <div className="attachmentrow">
                                    <div className="attach">
                                        <span className="titles">Attachments</span>
                                        <h3>Landing page.zip</h3>
                                    </div>
                                    <div className="attach">
                                        <span className="titles">Status</span>
                                        <FormControlLabel
                                        control={
                                        <Checkbox
                                            color="primary"
                                            inputProps={{ 'aria-label': 'secondary checkbox' }}
                                        />
                                            }
                                            label="APPROVE"
                                        />
                                        <FormControlLabel
                                        control={
                                        <Checkbox
                                            color="primary"
                                            inputProps={{ 'aria-label': 'secondary checkbox' }}
                                        />
                                            }
                                            label="DISCARD"
                                        />
                                    </div>
                                </div>
                                <div className="corporateviewdialogsubmitbtn">
                                    <Grid container>
                                        <Grid item sm={9}>
                                            <TextField 
                                            className="viewprojectdialogfield"
                                            placeholder="Meeting Link"
                                            // id={fields.firstNameField.name}
                                            // name={fields.firstNameField.name}
                                            // value={fields.firstNameField.value}
                                            // onChange={value => {
                                            //     textFieldChageHandler(
                                            //         fields.firstNameField.name,
                                            //         value
                                            //     )
                                            // }}
                                            variant="outlined" />
                                        </Grid>
                                        <Grid item sm={3}>
                                        <Button disabled={true} variant="contained" color="primary">
                                            Submit
                                        </Button>
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={12} md={5} className="leftgridpadding" >
                            <div className="datepicker">
                                <h2>Schedule Presentation</h2>
                                <span className="title" >Date</span>
                                <Calendar
                                onChange={this.onChange}
                                value={this.state.date}
                                />
                            </div>
                            <div className="timepicker">
                                <span className="title" >Time</span>
                                <TimePicker
                                onChange={this.onChange}
                                value={this.state.time}
                                />

                                <TimePicker
                                onChange={this.onChange}
                                value={this.state.time}
                                />
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </>
        )
    }
}

export default JobAICorporateViewProjectDialog;