export const styles = theme => ({
	flexGrow: {
		flex: 1,
		textAlign: "left"
	},
	active:{
		color: "#1AC960",
		textAlign: "center",
		display: "block",
	},
	inactive:{
		color: "#F83232",
		textAlign: "center",
		display: "block",
	},
	normal:{
		color: "#303030",
		textAlign: "center",
		display: "block",
	},
	corporatereviewertableoptions: {
		textAlign: "center",
		"& .MuiFormControl-root": {
			width: "81%",
			border: "1px solid #E0E0E0",
			background: "white",
			borderRadius: "3px",
		}
	},
	coporatereviwerdialognbtn: {
		margin: "auto",
		display: "block",
		marginTop: "1px !important",
		background: "none",
		boxShadow: "none !important",
		color: "#0080ff",
		fontWeight: "800",
		"&:hover": {
			background: "none",
		}
	}
});
