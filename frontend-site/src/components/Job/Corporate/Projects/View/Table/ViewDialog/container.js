import { connect } from "react-redux";
import JobAICorporateViewProjectDialog from "./component";
import instance, { CAPSTONE_BASE_URL } from "../../../../../../../api/config";
import {
    // getSubmitedProjectsTableData,
    isLoadingStateHandler
} from "../../../actions"

const mapStateToProps = state => ({
    viewProjects: state.CORPORATE_CAPSTONE.listData.Capstone.Corporate.Dialouges.viewProjects,
    isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
})

const mapDispatchToProps = (dispatch, props) => ({
    fetchDataIfNeeded: (value) => {
        dispatch(isLoadingStateHandler(true));
        instance.get(CAPSTONE_BASE_URL+'CandDetails/' + value +"/1")
        .then(res => {
            // dispatch(getSubmitedProjectsTableData(res.data));
            dispatch(isLoadingStateHandler(false));
        })
        .catch(error => {
            dispatch(isLoadingStateHandler(false));
        })
      }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    fetchDataIfNeeded: () => {
        dispatchProps.fetchDataIfNeeded(ownProps.viewDialogId);
    },
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(JobAICorporateViewProjectDialog);