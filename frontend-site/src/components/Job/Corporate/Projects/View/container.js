import { connect } from "react-redux";
import JobAICorporateViewProject from "./component";
import instance, { CAPSTONE_BASE_URL } from "../../../../../api/config";
import {
    getSubmitedProjectsTableData,
    isLoadingStateHandler
} from "../actions"

const mapStateToProps = state => ({
    submittedProjectsData: state.CORPORATE_CAPSTONE.listData.Capstone.Corporate.submittedProjects,
    isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
})

const mapDispatchToProps = (dispatch, props) => ({
    fetchDataIfNeeded: () => {
        dispatch(isLoadingStateHandler(true));
        instance.get(CAPSTONE_BASE_URL+'Submitted')
        .then(res => {
            dispatch(getSubmitedProjectsTableData(res.data));
            dispatch(isLoadingStateHandler(false));
        })
        .catch(error => {
            dispatch(isLoadingStateHandler(false));
        })
      }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(JobAICorporateViewProject);