import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import MUIDataTable from "mui-datatables";
import ColumnHeader from "./Table/ColumnHeader/container";
import ColumnBody from "./Table/ColumnBody/container";

class JobAICorporateAssignProjectPending extends Component {

    componentDidMount() {
        this.props.fetchDataIfNeeded();
    }
    setHandler = (value) => {
    }
    render() {
        const {
            pendigData,
            isLoading
        } = this.props;
        
        const columns = [
            {
             name: "email",
             label: "Email",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                    colWidth="210px"
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "id",
             label: "ID",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "project",
             label: "Project",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
            }
            },
            {
             name: "vertical",
             label: "Vertical",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
            }
            },
            {
             name: "level",
             label: "Level",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                    customBodyRender: (value, tableMeta, updateValue ) => (
                        <ColumnBody 
                            cellProps ={{
                                value: value,
                                tableMeta: tableMeta,
                                updateValue: updateValue,
                            }}
                        />
                    )
            }
            },
            
            {
                name: "actions",
                label: "Actions",
                options: {
                   filter: true,
                   sort: false,
                   customHeadRender: (props) => (
                       <ColumnHeader 
                           columnProps ={{
                               props: {...props},
                           }}
                       />
                       ),
                       customBodyRender: (value, tableMeta, updateValue ) => (
                        <ColumnBody 
                            cellProps ={{
                                value: value,
                                tableMeta: tableMeta,
                                updateValue: updateValue,
                            }}
                        />
                    )
               }
               },
           ];

        const data = pendigData;

        const options = {
        filterType: 'checkbox',
        print: false,
        download: false,
        viewColumns: false,
        filter: false,
        selectableRowsHeader: false,
        };

        return (
            <>
                <MUIDataTable
                data={data} 
                columns={columns} 
                options={options}
                />
            </>
        )
    }
}

export default JobAICorporateAssignProjectPending;