import { connect } from "react-redux";
import JobAICorporateAssignProjectPending from "./component";
import instance, { CAPSTONE_BASE_URL } from "../../../../../../api/config";
import {
    getPendingTableData,
    isLoadingStateHandler
} from "../../actions";

const mapStateToProps = state => ({
    pendigData: state.CORPORATE_CAPSTONE.listData.Capstone.Corporate.pending,
    isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
})

const mapDispatchToProps = (dispatch, props) => ({
    
    fetchDataIfNeeded: () => {
        dispatch(isLoadingStateHandler(true));
        instance.get(CAPSTONE_BASE_URL+'Pending')
        .then(res => {
            dispatch(getPendingTableData(res.data));
            dispatch(isLoadingStateHandler(false));
        })
        .catch(error => {
            dispatch(isLoadingStateHandler(false));
        })
    }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(JobAICorporateAssignProjectPending);