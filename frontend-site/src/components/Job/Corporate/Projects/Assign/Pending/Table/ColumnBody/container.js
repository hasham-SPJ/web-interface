import instance, { CAPSTONE_BASE_URL } from "../../../../../../../../api/config";
import {connect} from "react-redux";
import ColumnBody from "./component";
import {
  getPendingTableData,
  isLoadingStateHandler
} from "../../../../actions";

const mapStateToProps = state => ({
  isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
});

const mapDispatchToProps = (dispatch, props) => ({
  
  deletePerson: (value) => {
    dispatch(isLoadingStateHandler(true))
    instance.delete(CAPSTONE_BASE_URL+ value)
    .then(res => {
        instance.get(CAPSTONE_BASE_URL+'Pending')
        .then(res => {
            dispatch(getPendingTableData(res.data));
            dispatch(isLoadingStateHandler(false))
        })
        .catch(error => {
          dispatch(isLoadingStateHandler(false))
        })
      dispatch(isLoadingStateHandler(false));
    })
    .catch(error => {
        
    })
  },
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(ColumnBody);
