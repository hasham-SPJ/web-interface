import React, { Component } from "react";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { styles } from "./styles";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

class ColumnBody extends Component {
	componentDidMount() {
		// this.props.fetchDataIfNeeded();
	}

	componentDidUpdate() {}

	editPersonHandler = e => {
		// this.props.editPersonHandler(this.props.cellProps.tableMeta.rowIndex);
	};

	deletePerson = e => {
		// this.props.deletePersonHandler(this.props.cellProps.tableMeta.rowIndex);
	};

	render() {
		const { editPersonHandler, classes, cellProps, assignProjectHandler, isLoading, deletePerson, ...rest } = this.props;
		return cellProps.tableMeta.columnData.name === "actions" ? (
			<>
				<IconButton onClick={(e) => deletePerson(cellProps.tableMeta.rowData[0])}>
					<DeleteIcon />
				</IconButton>
			</>
		) : (
			<span className={classes.active}>{cellProps.value}</span>
		)
	}
}

ColumnBody.propTypes = {};

export default withStyles(styles)(ColumnBody);
