import { connect } from "react-redux";
import JobAICorporateAssignProjectOngoing from "./component";
import instance, { CAPSTONE_BASE_URL } from "../../../../../../api/config";
import {
    getOnGoingTableData
} from "../../actions";

const mapStateToProps = state => ({
    onGoingData: state.CORPORATE_CAPSTONE.listData.Capstone.Corporate.onGoing,
    isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
})

const mapDispatchToProps = (dispatch, props) => ({
    fetchDataIfNeeded: () => {
        instance.get(CAPSTONE_BASE_URL+'InProgress')
        .then(res => {
            dispatch(getOnGoingTableData(res.data));
        })
        .catch(error => {
            
        })
    }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(JobAICorporateAssignProjectOngoing);