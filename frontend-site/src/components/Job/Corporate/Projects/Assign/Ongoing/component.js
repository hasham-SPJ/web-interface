import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import MUIDataTable from "mui-datatables";
import ColumnHeader from "./Table/ColumnHeader/container";
import ColumnBody from "./Table/ColumnBody/container";

class JobAICorporateAssignProjectOngoing extends Component {

    componentDidMount() {
        this.props.fetchDataIfNeeded();
    }
    setHandler = (value) => {
    }
    render() {
        const {
            onGoingData,
        } = this.props;
        
        const columns = [
            {
             name: "name",
             label: "Name",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                    colWidth="210px"
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "id",
             label: "ID",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "kickoff",
             label: "Kickoff",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
            }
            },
            {
             name: "deadline",
             label: "Deadline",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
            }
            },
            {
             name: "timer",
             label: "Timer",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                    customBodyRender: (value, tableMeta, updateValue ) => (
                        <ColumnBody 
                            cellProps ={{
                                value: value,
                                tableMeta: tableMeta,
                                updateValue: updateValue,
                            }}
                        />
                    )
            }
            },
           ];

        const data = onGoingData;

        const options = {
        filterType: 'checkbox',
        print: false,
        download: false,
        viewColumns: false,
        filter: false,
        selectableRowsHeader: false,
        };

        return (
            <>
                <MUIDataTable
                data={data} 
                columns={columns} 
                options={options}
                />
            </>
        )
    }
}

export default JobAICorporateAssignProjectOngoing;