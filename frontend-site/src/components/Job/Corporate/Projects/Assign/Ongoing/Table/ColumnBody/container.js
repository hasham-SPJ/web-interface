import instance, { CAPSTONE_BASE_URL } from "../../../../../../../../api/config";
import {connect} from "react-redux";
import ColumnBody from "./component";
import {
  getAssignNewTableData,
  isLoadingStateHandler
} from "../../../../actions";

const mapStateToProps = state => ({
  isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
});

const mapDispatchToProps = (dispatch, props) => ({
  assignProjectHandler: (id, email, vertical, level) => {
    let val = {
      candId: email,
      reviewerId: "user@example.com",
      vertical: vertical,
      level: level
    }
    dispatch(isLoadingStateHandler(true));
    instance.post(CAPSTONE_BASE_URL, val)
    .then(res => {
        instance.get(CAPSTONE_BASE_URL+'Unassigned')
        .then(res => {
            dispatch(getAssignNewTableData(res.data));
        })
        .catch(error => {
            
        })
      dispatch(isLoadingStateHandler(false));
    })
    .catch(error => {
        
    })
  },
  fetchDataIfNeeded: () => {
    instance.get(CAPSTONE_BASE_URL+'Unassigned')
    .then(res => {
        dispatch(getAssignNewTableData(res.data));
    })
    .catch(error => {
        
    })
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(ColumnBody);
