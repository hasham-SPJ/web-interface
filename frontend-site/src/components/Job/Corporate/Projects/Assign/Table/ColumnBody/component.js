import React, { Component } from "react";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { styles } from "./styles";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

class ColumnBody extends Component {
	componentDidMount() {
		// this.props.fetchDataIfNeeded();
	}

	componentDidUpdate() {}

	render() {
		const { editPersonHandler, classes, cellProps, assignProjectHandler, isLoading, deletePerson, ...rest } = this.props;
		return cellProps.tableMeta.columnData.name === "actions" ? (
			<>
				<IconButton onClick={(e) => deletePerson(cellProps.tableMeta.rowData[0])}>
					<DeleteIcon />
				</IconButton>
			</>
		): (
			
			(cellProps.tableMeta.columnData.name === "vertical" || cellProps.tableMeta.columnData.name === "level" ) ? 
				<div className={classes.corporatereviewertableoptions}>
					{
						cellProps.tableMeta.columnData.name === "vertical" ? 
						<FormControl  className={classes.formControl}>
						{/* <InputLabel id="demo-simple-select-outlined-label">Select</InputLabel> */}
						<Select
						// labelId="demo-simple-select-outlined-label"
						// id="demo-simple-select-outlined"
						value={cellProps.tableMeta.rowData[2]}
						// onChange={handleChange}
						// label="Select"
						>
						<MenuItem selected={true} value={cellProps.tableMeta.rowData[2]}>
							{cellProps.tableMeta.rowData[2]}
						</MenuItem>
						<MenuItem value="Healthcare">Healthcare</MenuItem>
						<MenuItem value="Education">Education</MenuItem>
						<MenuItem value="IT">IT</MenuItem>
						<MenuItem value="Finance">Finance</MenuItem>
						<MenuItem value="IOT">IOT</MenuItem>
						<MenuItem value="Sport">Sport</MenuItem>
						</Select>
					</FormControl>
					:
					<FormControl  className={classes.formControl}>
						{/* <InputLabel id="demo-simple-select-outlined-label">Select</InputLabel> */}
						<Select
						labelId="demo-simple-select-outlined-label"
						id="demo-simple-select-outlined"
						value={cellProps.tableMeta.rowData[3]}
						// onChange={handleChange}
						// label="Select"
						>
						<MenuItem selected={true} value={cellProps.tableMeta.rowData[3]}>
							{cellProps.tableMeta.rowData[3]}
						</MenuItem>
						<MenuItem value="Beginner">Beginner</MenuItem>
						<MenuItem value="Intermediate">Intermediate</MenuItem>
						<MenuItem value="Expert">Expert</MenuItem>
						</Select>
					</FormControl>
					}
				</div> :
				(
					cellProps.tableMeta.columnData.name === "status" ? 
					<Button variant="contained" color="primary" onClick={(e) => assignProjectHandler(cellProps.tableMeta.rowData[1], cellProps.tableMeta.rowData[0], cellProps.tableMeta.rowData[2], cellProps.tableMeta.rowData[3])} className={classes.coporatereviwerassignbtn} >
						Assign
					</Button> : 
					<span className={classes.active}>{cellProps.value}</span>
				)
			
		)
	}
}

ColumnBody.propTypes = {};

export default withStyles(styles)(ColumnBody);
