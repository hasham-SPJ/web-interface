export const styles = theme => ({
	flexGrow: {
		flex: 1,
		textAlign: "left"
	},
	active:{
		color: "#303030",
		textAlign: "center",
		display: "block",
	},
	inactive:{
    color: "blue"
	},
	corporatereviewertableoptions: {
		textAlign: "center",
		"& .MuiFormControl-root": {
			width: "81%",
			border: "1px solid #E0E0E0",
			background: "white",
			borderRadius: "3px",
		}
	},
	coporatereviwerassignbtn: {
		marginTop: "1px !important",
    	margin: "auto",
    	display: "block",
	}
});
