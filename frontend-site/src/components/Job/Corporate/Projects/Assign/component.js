import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import JobAICorporateHeader from "../../Dashboard/Header/container";
import MUIDataTable from "mui-datatables";
import ColumnHeader from "./Table/ColumnHeader/container";
import ColumnBody from "./Table/ColumnBody/container";
import Dialog from "../../../../common/Dialog/component";
import JobAICorporateAssignProjectPending from "./Pending/container";
import JobAICorporateAssignProjectOngoing from "./Ongoing/container";

class JobAICorporateAssignProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 0,
        }
    }

    componentDidMount() {
        this.props.fetchDataIfNeeded();
    }
    setHandler = (value) => {
        this.setState({
            id: value,
        })
    }
    render() {
        const {
            assignNewData,
            isLoading
        } = this.props;
        
        const columns1 = [
            {
             name: "email",
             label: "Email",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                    colWidth="210px"
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "id",
             label: "ID",
             options: {
              filter: true,
              sort: true,
              customHeadRender: (props) => (
                <ColumnHeader 
                    columnProps ={{
                        props: {...props},
                    }}
                />
                ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
             }
            },
            {
             name: "vertical",
             label: "Vertical",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
            }
            },
            {
             name: "level",
             label: "Level",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                customBodyRender: (value, tableMeta, updateValue ) => (
                    <ColumnBody 
                        cellProps ={{
                            value: value,
                            tableMeta: tableMeta,
                            updateValue: updateValue,
                        }}
                    />
                )
            }
            },
            {
             name: "status",
             label: "Status",
             options: {
                filter: true,
                sort: false,
                customHeadRender: (props) => (
                    <ColumnHeader 
                        columnProps ={{
                            props: {...props},
                        }}
                    />
                    ),
                    customBodyRender: (value, tableMeta, updateValue ) => (
                        <ColumnBody 
                            cellProps ={{
                                value: value,
                                tableMeta: tableMeta,
                                updateValue: updateValue,
                            }}
                        />
                    )
            }
            },
           ];
           
        // const columns = ["Name", "ID", "Parameter X", "Parameter Y", "Vertical", "Level", "Status"];

        const data1 = assignNewData;

        const data2 = [
            ["Joe James", "1", "April 13, 2020", "April 13, 2020", ""],
            ["John Walsh", "2", "April 13, 2020", "April 13, 2020", ""],
            ];

        const options = {
        filterType: 'checkbox',
        print: false,
        download: false,
        viewColumns: false,
        filter: false,
        selectableRowsHeader: false,
        };

        return (
            <>
                <Dialog open={isLoading} isloadingBar={true} message="Loading Data" />
                <JobAICorporateHeader />
                <div className="jobaicorporateassign">
                    <Grid container justify="center">
                        <Grid item sm={10}>
                            <div className="corporateassignoptions">
                                <Button onClick={(e) => this.setHandler(0)} className={this.state.id === 0 ? "active" : null } variant="contained" color="primary">
                                    Assign New
                                </Button>
                                <Button onClick={(e) => this.setHandler(2)} className={this.state.id === 2 ? "active" : null } variant="contained" color="primary">
                                    Pending Acceptance
                                </Button>
                                <Button onClick={(e) => this.setHandler(1)} className={this.state.id === 1 ? "active" : null } variant="contained" color="primary">
                                    Ongoing
                                </Button>
                            </div>
                        </Grid>
                    </Grid>

                    <Grid container justify="center">
                        <Grid item sm={10} className="jobaicorporatetables">
                            {
                                this.state.id === 0 ? 
                                <>
                                    <MUIDataTable
                                    data={data1} 
                                    columns={columns1} 
                                    options={options}
                                    />
                                </> : 
                                this.state.id === 1 ? 
                                <>
                                    <JobAICorporateAssignProjectOngoing />
                                </> : <JobAICorporateAssignProjectPending />
                            }
                        </Grid>
                    </Grid>
                </div>
            </>
        )
    }
}

export default JobAICorporateAssignProject;