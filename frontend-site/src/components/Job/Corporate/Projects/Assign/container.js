import { connect } from "react-redux";
import JobAICorporateAssignProject from "./component";
import instance, { CAPSTONE_BASE_URL } from "../../../../../api/config";
import {
    getAssignNewTableData
} from "../actions";

const mapStateToProps = state => ({
    assignNewData: state.CORPORATE_CAPSTONE.listData.Capstone.Corporate.assignNew,
    isLoading: state.CORPORATE_CAPSTONE.UISetting.isLoading,
})

const mapDispatchToProps = (dispatch, props) => ({
    fetchDataIfNeeded: () => {
        instance.get(CAPSTONE_BASE_URL+'Unassigned')
        .then(res => {
            dispatch(getAssignNewTableData(res.data));
        })
        .catch(error => {
            
        })
    }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(JobAICorporateAssignProject);