import React, {Component} from 'react';
import "./style.css";
import { Button, Typography, Grid }  from '@material-ui/core';
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
class CapstoneSelectTime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isTimeSelected: false,
            isconfirmSelected: false,
        }
    }
    isTimeSelectedHandler = () => {
        this.setState({
            isTimeSelected: true,
        })
    }
    handleDateChange = () => {

    }
    confirmTimehandler = () => {
        this.setState({
            isconfirmSelected: true,
        })
    }
    render() {
        const {
            score,
            nextSubmitHandler
        }  = this.props;
        return (
            <>
            <div className="jobaicapstoneselectwrapper">
                    <h1><CheckCircleOutlineIcon /> Test Submitted Successfully</h1>
                        {
                            this.state.isTimeSelected ? 
                        <span>Will Get Back to You in: 1day 30 Minutes</span> : null }
                   {this.state.isconfirmSelected ?  
                   <Button href="/" variant="contained" color="primary" className="capstonefinalnextbtn">
                        Next
                    </Button>
                     : <>
                    <h2>Select Capstone Project Display/Presentation Meeting Time (GMT +05:00)</h2>
                    <div className="capstoneslectitmeform">
                        <div className="capstoneselectimeformrow">
                                <span className="meetingdatetitle">
                                    Meeting Date
                                </span>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            // variant="outlined"
                                            format="MM/dd/yyyy"
                                            margin="normal"
                                            id="date-picker-inline"
                                            onChange={this.handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                            className="capstonemeetimeselcetor"
                                        />
                                    </MuiPickersUtilsProvider>
                                    <span className="meetingdatetitlespace">
                                            to
                                    </span>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            // variant="outlined"
                                            format="MM/dd/yyyy"
                                            margin="normal"
                                            id="date-picker-inline"
                                            onChange={this.handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                            className="capstonemeetimeselcetor"
                                        />
                                    </MuiPickersUtilsProvider>
                        </div>

                        <div className="capstoneselectimeformrow">
                                <span className="meetingdatetitle">
                                    Meeting Time
                                </span>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardTimePicker
                                            margin="normal"
                                            id="time-picker"
                                            onChange={this.handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                            className="capstonemeetimeselcetor"
                                        />
                                    </MuiPickersUtilsProvider>
                                    <span className="meetingdatetitlespace">
                                            to
                                    </span>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardTimePicker
                                            margin="normal"
                                            id="time-picker"
                                            onChange={this.handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                            className="capstonemeetimeselcetor"
                                        />
                                    </MuiPickersUtilsProvider>
                        </div>
                        <h3>Note:</h3>
                        <p>Once Meeting Date and Time will be confirmed it can't be cancel.</p>
                    </div>
                </> }
            </div>

            <div className="capstonenextcancelbtn">
                    <Button disabled={this.state.isconfirmSelected ? true : false} onClick={this.state.isTimeSelected ? this.confirmTimehandler : this.isTimeSelectedHandler} variant="contained" color="primary" >
                        {
                            this.state.isTimeSelected ? "Confirm" : "Next" 
                        }
                    </Button>
                    <Button disabled={this.state.isconfirmSelected ? true : false} variant="contained" color="primary" >
                        Cancel
                    </Button>
            </div>
            </>
        )
    }
}

export default CapstoneSelectTime;