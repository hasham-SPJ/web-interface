import { connect } from "react-redux";
import CandidateCapstoneProject from "./component";
import {
    capstoneProjectStatusHandler
} from "../actions";

const mapStateToProps = state => ({
    capstoneproject: state.CANDIDATE.data.tests.capstoneproject,

})

const mapDispatchToProps = (dispatch, props) => ({
    capstoneProjectStatusHandler: () => {
        dispatch(capstoneProjectStatusHandler("inprogress"));
    },
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(CandidateCapstoneProject);