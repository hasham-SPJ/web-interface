import React, { Component } from "react";
import "./style.css";
import Button from '@material-ui/core/Button';
import QueryBuilderIcon from "@material-ui/icons/QueryBuilder";
import DesktopMacicon from "@material-ui/icons/DesktopMac";
import CapstoneSelectTime from "./SelectTime/container";
import ImportContactsicon from "@material-ui/icons/ImportContacts";
import GetAppIcon from "@material-ui/icons/GetApp";

class CandidateCapstoneProject extends Component {

    fileHandler = (e) => {
        // let zip = require('jszip')();
        // let files = e.target.files;
        // let file = files[0];
        // let allZip =  zip.file(file.name, file);
        // console.log("files", allZip);
    }
    render() {
        const {
            capstoneproject,
            capstoneProjectStatusHandler
        } = this.props;

        return (
            <div className="capstoneprojectwrapper">

                {capstoneproject.test.status === "pending" ? 
                <>
                    <h3>Capstone Project</h3>
                    <div className="titlewithicon">
                        <QueryBuilderIcon />
                        <span>Time Left: 10:00</span>
                    </div>
                    <p>

                    <div className="titlewithicon">
                        <DesktopMacicon />
                        <span>Instructions</span>
                    </div> <br></br>
                    Your Capstone project will be recorded in recruitment process of <span>JobAi</span> and will be a permanent record of project and its findings – therefore it’s important to present your project in the best possible way to get filtered in. We strongly encourage the use of appropriate media such as images, video and audio to increase the impact of your dissemination Please find the attached Capstone Project also check out the time duration.
                    </p>

                    <p>

                    <a href="#" className="capstonedownloadlink" download>Capstone-Project.pdf <GetAppIcon /></a>
                    <div className="titlewithicon">
                        <ImportContactsicon />
                        <span>Final work files</span>
                    </div> <br></br>
                    Final work files If you have files related to your final work (such as a final report, photos) that you wish to have displayed alongside your description, please upload here. Maximum 5 files, maximum 20 MB per file. Can't upload? If you have other file types, oversized files (i.e. video, audio, etc) or can't upload your files contact help center <span>johndoe@helpdesk.com</span>.
                    </p>

                    <div className="capstonedropfileare">
                        <p>Drop your file(s) here or</p>
                        <input
                                accept="*"
                                id="contained-button-file"
                                multiple
                                onChange={(e) => this.fileHandler(e)}
                                type="file"
                            />
                            <label htmlFor="contained-button-file" className="capstonefileupload">
                                <Button className="" variant="contained" color="primary" component="span">
                                    Select File
                                </Button>
                            </label>
                    </div>
                    <p>Accepted file types: jpg, jpeg, gif, png, pdf.</p>
                    <Button onClick={capstoneProjectStatusHandler} variant="contained" color="primary" className="capstonenextbtn"
                     >
                        Next
                    </Button>
                </>
                : null }
                {
                    capstoneproject.test.status === "inprogress" ? <CapstoneSelectTime  /> : null
                }
            </div>
        )
    }
}

export default CandidateCapstoneProject;