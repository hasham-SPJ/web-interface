import { connect } from "react-redux";
import instance, { TEST_BASE_URL } from "../../../api/config";
import JobAIStartTest from "./component";
import {
    getAttitudeTest,
    getEITTest
} from "../actions";
const mapStateToProps = state => ({
    eittest: state.CANDIDATE.data.tests.eittest,
    CVFlag: state.CANDIDATE.forms.screeningCVForm.submitFlag,
})

const mapDispatchToProps = (dispatch, props) => ({
    fetchDataIfNeeded: () => {
        instance.get(TEST_BASE_URL+'AttitudeTest')
        .then(res => {
            dispatch(getAttitudeTest(res.data));
        })
        .catch(error => {
            
        })

        instance.get(TEST_BASE_URL+'EmotionalIntelligence')
        .then(res => {
            dispatch(getEITTest(res.data));
        })
        .catch(error => {
            
        })
    }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(JobAIStartTest);