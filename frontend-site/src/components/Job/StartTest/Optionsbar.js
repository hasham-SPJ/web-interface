import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ScreeningTest from "../Screening/container";
import HardSkillTest from "../HardSkillsTest/container";
import EITTest from "../EITTest/container";
import ScreeningA from "../ScreeningA/container";
import AttitudeTest from "../AttitudeTest/container";
import ScreenignAIcong from "../../../icons/screeninga.png";
import ScreenignBIcong from "../../../icons/screeningb.png";
import HardSkillTestIcong from "../../../icons/hardskilltest.png";
import SoftSkillTestIcong from "../../../icons/softskilltest.png";
import ClassificaionIcong from "../../../icons/classification.png";
import TrainingIcong from "../../../icons/training.png";
import GraduationIcong from "../../../icons/graduation.png";
import CapstonProjectIcong from "../../../icons/capstoneproject.png";
import CertificationIcong from "../../../icons/certification.png";
import OnlineRankingIcong from "../../../icons/onlineranking.png";
import MarketPlaceIcong from "../../../icons/marketplace.png";
import SmartRecruitingIcong from "../../../icons/smartrecruiting.png";
import NotFound from "../../common/NotFound/component";
import CandidateCapstoneProject from "../CapstoneProject/container";
import CandidateCVSubmission from "../CVSubmission/container";

function TabPanel(props) {
  const { children, value, isEITSTART, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: "17px",
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    "& .MuiTab-wrapper": {
      "& img": {
        maxWidth: 52,
      }
    },
    "& header": {
      boxShadow: "none",
    }
  },
  tabs: {
    // color: "#3f51b5",
    boxShadow: "0px 0px 14px -3px #00000040",
    background: "none",
    borderRadius: "19px",
    margin: "19px",
    minHeight: "133px",
  }
}));

export default function ScrollableTabsButtonAuto(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  const { isEITSTART, CVFlag } = props;

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="on"
          aria-label="scrollable auto tabs example"
        >
          <Tab className={classes.tabs} label="Screening A" icon={<img src={ScreenignAIcong} /> } {...a11yProps(0)} > </Tab>
          <Tab className={classes.tabs} label="Video Screening" icon={<img src={ScreenignBIcong} /> } {...a11yProps(1)} />
          <Tab className={classes.tabs} label="HardSkill Test" icon={<img src={HardSkillTestIcong} /> } {...a11yProps(2)} />
          <Tab className={classes.tabs} label="SoftSkill Test" icon={<img src={SoftSkillTestIcong} /> } {...a11yProps(3)} />
          <Tab className={classes.tabs} disabled={true} label="Classification" icon={<img src={ClassificaionIcong} /> } {...a11yProps(4)} />
          <Tab className={classes.tabs} disabled={true} label="Training" icon={<img src={TrainingIcong} /> } {...a11yProps(5)} />
          <Tab className={classes.tabs} disabled={true} label="Graduation" icon={<img src={GraduationIcong} /> } {...a11yProps(6)} />
          <Tab className={classes.tabs} label="Capstone Project" icon={<img src={CapstonProjectIcong} /> } {...a11yProps(7)} />
          <Tab className={classes.tabs} disabled={true} label="Certification" icon={<img src={CertificationIcong} /> } {...a11yProps(8)} />
          <Tab className={classes.tabs} disabled={true} label="Online Ranking" icon={<img src={OnlineRankingIcong} /> } {...a11yProps(9)} />
          <Tab className={classes.tabs} disabled={true} label="Market Place" icon={<img src={MarketPlaceIcong} /> } {...a11yProps(10)} />
          <Tab className={classes.tabs} disabled={true} label="Smart Recruiting" icon={<img src={SmartRecruitingIcong} /> } {...a11yProps(11)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <ScreeningA />
        {/* <CandidateCVSubmission /> */}
      </TabPanel>
      <TabPanel value={value} index={1}>
        <ScreeningTest />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <HardSkillTest />
      </TabPanel>
      <TabPanel value={value} index={3}>
        {CVFlag ? 
        <>
          {isEITSTART ? <EITTest /> : 
          <AttitudeTest /> }
        </> : <NotFound title="Please submit CV to Proceed" />  
        }
        {/* <EITTest /> */}
      </TabPanel>
      <TabPanel value={value} index={4}>
        Classification
      </TabPanel>
      <TabPanel value={value} index={5}>
        Training
      </TabPanel>
      <TabPanel value={value} index={6}>
        Graduation
      </TabPanel>
      <TabPanel value={value} index={7}>
        <CandidateCapstoneProject />
      </TabPanel>
      <TabPanel value={value} index={8}>
        Certification
      </TabPanel>
      <TabPanel value={value} index={9}>
        Online Ranking
      </TabPanel>
      <TabPanel value={value} index={10}>
        Market Place
      </TabPanel>
      <TabPanel value={value} index={11}>
        Smart Recruiting
      </TabPanel>
    </div>
  );
}