import React, {Component} from 'react';
import "./style.css";
import { Button, Typography, Grid }  from '@material-ui/core';
import JobAiBg from "../../../draft.gif";
import ScrollBarTestOpt from "./Optionsbar";

class JobAIStartTest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startTest : true,
        }
    }
    scrollToTest = () => {
        window.scrollTo(null, 700);
    }
    startTesthandler = () => {
        this.setState({
            startTest : false,
        });
    }
    componentDidMount() {
        this.props.fetchDataIfNeeded();
    }
    render() {
        const {
            eittest,
            CVFlag
        } = this.props;
        return (
            <>
            {
                this.state.startTest ? 
            
            <div className="jobaistarttestpage">
                <Grid container>
                    <Grid item xs={12} sm={12} md={12}>
                    <div className="jobaistarttestwrapper"> 
                        <h1>How It Works?</h1>
                        <img src={JobAiBg} ></img>
                        <Button onClick={this.startTesthandler} variant="outlined" color="primary">
                            Start Testing
                        </Button>                                             
                    </div>
                    </Grid>
                </Grid>
            </div> : 

            <div className="jobaistarttestopt" ref="test">
                <Grid container>
                    <Grid item xs={12} sm={12} md={12}>
                        <ScrollBarTestOpt isEITSTART={eittest.test.isStart} CVFlag={CVFlag}/>
                    </Grid>
                </Grid>
            </div> }
            </>
        )
    }
}

export default JobAIStartTest;