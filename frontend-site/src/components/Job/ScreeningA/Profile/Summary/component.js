import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import EditIcon from "@material-ui/icons/Edit";
import PersonIcon from "@material-ui/icons/Person";

class CandidateScreeningAProfileSummary extends Component {

    render() {

        return (
            <>
                <div className="screeningAprofilesummarywrapper">
                    <div className="screeningAHeader">
                        <p>Profile Summary</p>
                        <EditIcon />
                    </div>
                    <div className="summarycontent">

                        {/* profile summary content starts here */}

                        <div className="profilesummarycontent">
                            <Grid container>
                                <Grid item sm={12} md={5} lg={4}>
                                    <div className="imgarea">
                                        <EditIcon className="loadsummary"/>
                                        <PersonIcon className="imgcontent"/>
                                        <p><span>Mobile Number:</span>  +123-456-789</p>
                                        <p><span>Email:</span> johndeo@example.com</p>
                                    </div>
                                </Grid>

                                <Grid item sm={12} md={7} lg={8}>
                                    <div className="namedetailarea">
                                        <h3>John Deo</h3>
                                        <div className="titles">
                                            <p>Father Name:</p>
                                            <span>Deo Orhim</span>
                                        </div>
                                        <div className="titles">
                                            <p>Age:</p>
                                            <span>24 Years 3 Months</span>
                                        </div>
                                        <div className="titles">
                                            <p>Gender:</p>
                                            <span>Male</span>
                                        </div>
                                        <div className="titles">
                                            <p>Martial Status:</p>
                                            <span>Unavailable</span>
                                        </div>
                                        <div className="titles">
                                            <p>Work Experience:</p>
                                            <span>Unavailable</span>
                                        </div>
                                        <div className="titles">
                                            <p>Industry:</p>
                                            <span>Information Technology</span>
                                        </div>
                                        <div className="titles">
                                            <p>Specialization:</p>
                                            <span>UI/UX Designer</span>
                                        </div>
                                        <div className="titles">
                                            <p>Country:</p>
                                            <span>Pakistan</span>
                                        </div>
                                        <div className="titlebelow">
                                            <p>Professional Summary</p>
                                            <span>Briefly describe your professional and academic background.</span>
                                        </div>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default CandidateScreeningAProfileSummary;