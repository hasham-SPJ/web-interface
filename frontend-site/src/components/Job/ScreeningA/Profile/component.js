import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import ContactDetail from "../ContactDetail/container";
import CandidateScreeningAProfileSummary from "./Summary/container";
import CandidateScreeningAcademic from "../Academic/container";
import CandidateScreeningWorkExperience from "../WorkExperience/container";

class CandidateScreeningAProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 0,
        }
    }

    selectedBlock = (value) => {
        this.setState({
            id: value,
        })
    }

    render() {
        
        return (
            <>
            <div className="screeningAprofilewrapper">
                <Grid container>
                    <Grid item sm={12} md={3} lg={2}>
                        <div className="screeningAsidebar">
                            <ul>
                                <li onClick={(e) => this.selectedBlock(0) } className={this.state.id === 0 ? "active" : null} >Home</li>
                                <li onClick={(e) => this.selectedBlock(1) } className={this.state.id === 1 ? "active" : null} >Contact Detail</li>
                                <li onClick={(e) => this.selectedBlock(2) } className={this.state.id === 2 ? "active" : null} >Academic Background</li>
                                <li onClick={(e) => this.selectedBlock(3) } className={this.state.id === 3 ? "active" : null} >Working Experience</li>
                            </ul>
                        </div>
                    </Grid>

                    <Grid item sm={12} md={9} lg={10}>
                        {
                            this.state.id === 0 ? <CandidateScreeningAProfileSummary /> : null
                        }
                        {
                            this.state.id === 1 ? <ContactDetail /> : null
                        }
                        {
                            this.state.id === 2 ? <CandidateScreeningAcademic /> : null
                        }
                        {
                            this.state.id === 3 ? <CandidateScreeningWorkExperience /> : null
                        }
                    </Grid>
                </Grid>
            </div>
            </>
        )
    }
}

export default CandidateScreeningAProfile;