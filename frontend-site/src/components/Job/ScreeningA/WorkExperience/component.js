import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import ProfileImg from "../../../../profileimg.png";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import ColorCircularProgress from '@material-ui/core/LinearProgress';
import PersonIcon from "@material-ui/icons/Person";
import EditIcon from "@material-ui/icons/Edit";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";

class CandidateScreeningWorkExperience extends Component {

    render() {
        
        return (
            <div className="screeningacontactdetailwrapper">
                {/* <div className="screeningacontactdetail">
                    <div className="screeningAHeader">
                        <p>Academic Background</p>
                        <EditIcon />
                    </div>
                </div> */}
                
                <div className="addacademicform">
                <Grid container>
                    
                    <Grid item xs={12} md={9}>
                    <div className="addacademicformfields">
                        <div className="screeningAHeader">
                            <p>Work Experience</p>
                        </div>
                        <Grid container className="screeningwrapperpadding">
                            <form className="screeningaform" noValidate autoComplete="off">
                                <Grid item xs={12} sm={12} lg={12}>
                                        <FormControl className="screeningafieldfull" >
                                            <InputLabel id="degree-level-select-label">Degree Level</InputLabel>
                                            <Select
                                                labelId="degree-level-select-label"
                                                // id={fields.genderField.name}
                                                // name={fields.genderField.name}
                                                // value={fields.genderField.value}
                                                // onChange={value => {
                                                //     selectFieldChageHandler(
                                                //         fields.genderField.name,
                                                //         value
                                                //     )
                                                // }}
                                            >
                                                <MenuItem value={1}>1</MenuItem>
                                                <MenuItem value={2}>2</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <TextField 
                                        className="screeningafieldfull"
                                        // id={fields.lastNameField.name}
                                        // name={fields.lastNameField.name}
                                        // value={fields.lastNameField.value}
                                        // onChange={value => {
                                        //     textFieldChageHandler(
                                        //         fields.lastNameField.name,
                                        //         value
                                        //     )
                                        // }}
                                        label="Degree" />
                                </Grid>
                                <Grid item xs={12} sm={6} lg={6}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            variant="inline"
                                            format="MM/dd/yyyy"
                                            margin="normal"
                                            id="date-picker-inline"
                                            label="Starting Year"
                                            // onChange={handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                            className="screeningafieldfull"
                                        />
                                    </MuiPickersUtilsProvider>
                                </Grid>
                                <Grid item xs={12} sm={6} lg={6}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            variant="inline"
                                            format="MM/dd/yyyy"
                                            margin="normal"
                                            id="date-picker-inline"
                                            label="Ending Year"
                                            // onChange={handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                            className="screeningafieldfull"
                                        />
                                    </MuiPickersUtilsProvider>
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <TextField
                                        className="screeningafieldfull"
                                        // id={fields.emailField.name}
                                        // name={fields.emailField.name}
                                        // value={fields.emailField.value}
                                        // onChange={value => {
                                        //     textFieldChageHandler(
                                        //         fields.emailField.name,
                                        //         value
                                        //     )
                                        // }}
                                        label="Institution" />
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <FormControl className="screeningafieldfull" >
                                        <InputLabel id="degree-level-select-label">Country</InputLabel>
                                        <Select
                                            labelId="degree-level-select-label"
                                            // id={fields.genderField.name}
                                            // name={fields.genderField.name}
                                            // value={fields.genderField.value}
                                            // onChange={value => {
                                            //     selectFieldChageHandler(
                                            //         fields.genderField.name,
                                            //         value
                                            //     )
                                            // }}
                                        >
                                            <MenuItem value={"Pakistan"}>Pakistan</MenuItem>
                                            <MenuItem value={"Greece"}>Greece</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <FormControl className="screeningafieldfull" >
                                        <InputLabel id="degree-level-select-label">State</InputLabel>
                                        <Select
                                            labelId="degree-level-select-label"
                                            // id={fields.genderField.name}
                                            // name={fields.genderField.name}
                                            // value={fields.genderField.value}
                                            // onChange={value => {
                                            //     selectFieldChageHandler(
                                            //         fields.genderField.name,
                                            //         value
                                            //     )
                                            // }}
                                        >
                                            <MenuItem value={"Pakistan"}>Pakistan</MenuItem>
                                            <MenuItem value={"Greece"}>Greece</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <TextField
                                        className="screeningafieldfull"
                                        // id={fields.addressField.name}
                                        // name={fields.addressField.name}
                                        // value={fields.addressField.value}
                                        // onChange={value => {
                                        //     textFieldChageHandler(
                                        //         fields.addressField.name,
                                        //         value
                                        //     )
                                        // }}
                                        label="City" />
                                </Grid>

                                <Grid container justify="center">
                                    <Grid item md={4}>
                                        <Button className="screeningasavebtn" variant="contained" color="primary">
                                            Add
                                        </Button>
                                        
                                    </Grid>
                                </Grid>
                            </form>
                        </Grid> 
                        </div>
                    </Grid>
                    
                    
                        <Grid item xs={12} md={3}>
                            <div className="addacademicdataseries">

                                <div className="seriesbox">
                                    <div className="seriesboxcontent">
                                        <span>Currently Working</span>
                                        <p>Ui/Ux Designer</p>
                                        <h3>Softdac Technologies.</h3>
                                        <span>Lahore, Pakistan</span>
                                        <RadioButtonCheckedIcon />
                                    </div>
                                    <EditIcon className="mainedit"/>
                                </div>
                                <div className="seriesbox">
                                    <div className="seriesboxcontent">
                                        <span>Currently Working</span>
                                        <p>Ui/Ux Designer</p>
                                        <h3>Softdac Technologies.</h3>
                                        <span>Lahore, Pakistan</span>
                                        <RadioButtonCheckedIcon />
                                    </div>
                                    <EditIcon className="mainedit"/>
                                </div>
                            </div>
                        </Grid>
                   
                </Grid>
                </div>
            </div>
        )
    }
}

export default CandidateScreeningWorkExperience;