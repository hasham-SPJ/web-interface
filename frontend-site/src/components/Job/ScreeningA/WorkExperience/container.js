import { connect } from "react-redux";
import CandidateScreeningWorkExperience from "./component";

const mapStateToProps = state => ({
    fields: state.CANDIDATE.forms.screeningCVForm.fields,
    forms: state.CANDIDATE.forms,
})

const mapDispatchToProps = (dispatch, props) => ({

})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(CandidateScreeningWorkExperience);