import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import CandidateScreeningAContacDetail from "./ContactDetail/container";

class CandidateScreeningA extends Component {

    render() {
        
        return (
            <>
            {/* <CandidateScreeningAContacDetail /> */}
            <div className="screeningAnewwrapper">
                <div className="screeningAwelcome">
                    <h1>Welcome John Doe</h1>
                    <p>Complete your profile</p>
                    <Button href="/jobai/candiate-profile" variant="contained" color="primary">
                        Proceed
                    </Button>
                </div>

            </div>
            </>
        )
    }
}

export default CandidateScreeningA;