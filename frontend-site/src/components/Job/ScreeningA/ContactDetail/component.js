import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import ProfileImg from "../../../../profileimg.png";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import ColorCircularProgress from '@material-ui/core/LinearProgress';
import PersonIcon from "@material-ui/icons/Person";
import EditIcon from "@material-ui/icons/Edit";
import ImageCheckTrue from "../../../../icons/profile2.png";
import ImageCheckFalse from "../../../../icons/profile1.png";
import PhotoCameraIcon from "@material-ui/icons/PhotoCamera";

class CandidateScreeningAContacDetail extends Component {

    render() {
        const {
            fields,
            textFieldChageHandler,
            selectFieldChageHandler,
            submitScreeningCV,
            forms
        } = this.props;
        
        return (
            <div className="screeningacontactdetailwrapper">
                <div className="screeningacontactdetail">
                    <div className="screeningAHeader">
                        <p>Contact Detail</p>
                        <EditIcon />
                    </div>
                        <div className="contacdetailarea">
                            <Grid container justify="center">
                                <Grid item sm={12} md={5} lg={4}>
                                    <div className="imgarea">
                                        <PersonIcon className="imgcontent"/>
                                        <p>Maximum size of file 1MB <br />Face must be visible and clear</p>
                                        <p>Upload a professional and respectful headshots <br />Face must be visible and clear</p>
                                        <img src={ImageCheckTrue} className="imgcheck1"></img>
                                        <img src={ImageCheckFalse} className="imgcheck2"></img>
                                        <Button disabled={true} variant="contained" color="primary">
                                            Proceed
                                        </Button>
                                        <div className="uploadimgbutton">
                                            <PhotoCameraIcon />
                                            <span>Upload</span>
                                        </div>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                </div>
                
                {/* <Grid container>
                    <Grid item xs={12} md={9}>
                        <Grid container className="screeningwrapperpadding">
                            <form className="screeningaform" noValidate autoComplete="off">
                                <Grid item xs={12} sm={6} lg={6}> 
                                    <TextField 
                                        className="screeningafieldfull"
                                        id={fields.firstNameField.name}
                                        name={fields.firstNameField.name}
                                        value={fields.firstNameField.value}
                                        onChange={value => {
                                            textFieldChageHandler(
                                                fields.firstNameField.name,
                                                value
                                            )
                                        }}
                                        label="First Name" />
                                </Grid>
                                <Grid item xs={12} sm={6} lg={6}>
                                    <TextField 
                                        className="screeningafieldfull"
                                        id={fields.lastNameField.name}
                                        name={fields.lastNameField.name}
                                        value={fields.lastNameField.value}
                                        onChange={value => {
                                            textFieldChageHandler(
                                                fields.lastNameField.name,
                                                value
                                            )
                                        }}
                                        label="Last Name" />
                                </Grid>
                                <Grid item xs={12} sm={6} lg={6}>
                                    <TextField 
                                        className="screeningafieldfull"
                                        id={fields.fatherNameField.name}
                                        name={fields.fatherNameField.name}
                                        value={fields.fatherNameField.value}
                                        onChange={value => {
                                            textFieldChageHandler(
                                                fields.fatherNameField.name,
                                                value
                                            )
                                        }}
                                        label="Father name" />
                                </Grid>
                                <Grid item xs={12} sm={6} lg={6}>
                                    <TextField 
                                        className="screeningafieldfull"
                                        id={fields.cnicField.name}
                                        name={fields.cnicField.name}
                                        value={fields.cnicField.value}
                                        onChange={value => {
                                            textFieldChageHandler(
                                                fields.cnicField.name,
                                                value
                                            )
                                        }}
                                        label="CNIC" />
                                </Grid>
                                <Grid item xs={12} sm={6} lg={6}>
                                    <FormControl className="screeningafieldfull" >
                                        <InputLabel id="demo-simple-select-label">Gender</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id={fields.genderField.name}
                                            name={fields.genderField.name}
                                            value={fields.genderField.value}
                                            onChange={value => {
                                                selectFieldChageHandler(
                                                    fields.genderField.name,
                                                    value
                                                )
                                            }}
                                        >
                                            <MenuItem value={"Male"}>Male</MenuItem>
                                            <MenuItem value={"Female"}>Female</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12} sm={6} lg={6}>
                                    <FormControl className="screeningafieldfull" >
                                        <InputLabel id="demo-simple-select-label">Marital Status</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id={fields.maritalStatusField.name}
                                            name={fields.maritalStatusField.name}
                                            value={fields.maritalStatusField.value}
                                            onChange={value => {
                                                selectFieldChageHandler(
                                                    fields.maritalStatusField.name,
                                                    value
                                                )
                                            }}
                                        >
                                            <MenuItem value={"Single"}>Single</MenuItem>
                                            <MenuItem value={"Married"}>Married</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            variant="inline"
                                            format="MM/dd/yyyy"
                                            margin="normal"
                                            id="date-picker-inline"
                                            label="Date picker inline"
                                            // onChange={handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                            className="screeningafieldfull"
                                        />
                                    </MuiPickersUtilsProvider>
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <TextField
                                        className="screeningafieldfull"
                                        id={fields.emailField.name}
                                        name={fields.emailField.name}
                                        value={fields.emailField.value}
                                        onChange={value => {
                                            textFieldChageHandler(
                                                fields.emailField.name,
                                                value
                                            )
                                        }}
                                        label="Email" />
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <TextField
                                        className="screeningafieldfull"
                                        id={fields.mobileNumberField.name}
                                        name={fields.mobileNumberField.name}
                                        value={fields.mobileNumberField.value}
                                        onChange={value => {
                                            textFieldChageHandler(
                                                fields.mobileNumberField.name,
                                                value
                                            )
                                        }}
                                        label="Mobile" />
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <TextField
                                        className="screeningafieldfull"
                                        id={fields.addressField.name}
                                        name={fields.addressField.name}
                                        value={fields.addressField.value}
                                        onChange={value => {
                                            textFieldChageHandler(
                                                fields.addressField.name,
                                                value
                                            )
                                        }}
                                        label="Address" />
                                </Grid>
                                <Grid item xs={12} sm={12} lg={12}>
                                    <TextField
                                        className="screeningafieldfull"
                                        id={fields.professionalSummaryField.name}
                                        name={fields.professionalSummaryField.name}
                                        value={fields.professionalSummaryField.value}
                                        onChange={value => {
                                            textFieldChageHandler(
                                                fields.professionalSummaryField.name,
                                                value
                                            )
                                        }}
                                        label="Professional Summary" />
                                </Grid>

                                <Grid container justify="center">
                                    <Grid item md={4}>
                                        <Button className="screeningasavebtn" variant="contained" color="primary">
                                            Cancel
                                        </Button>
                                    </Grid>
                                    <Grid item md={4}>
                                        {forms.screeningCVForm.submitFlag ? 
                                        <Button className="screeningasavebtn" variant="contained" color="primary">
                                        Edit
                                    </Button> : 
                                    <Button className="screeningasavebtn" onClick={submitScreeningCV} variant="contained" color="primary">
                                        Save
                                    </Button> }
                                        
                                    </Grid>
                                </Grid>
                            </form>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} md={3}>
                        <div className="screeningaright">
                            <img src={ProfileImg} alt="Profile Picture"></img>
                            <input
                                accept="image/*"
                                id="contained-button-file"
                                multiple
                                type="file"
                            />
                            <label htmlFor="contained-button-file">
                                <Button className="screeningimgupload" variant="contained" color="primary" component="span">
                                    Update Profile Image
                                </Button>
                            </label>

                            <div className="screeningaprogress">
                                <h2>Personal Information</h2>
                                <ColorCircularProgress
                                variant="determinate"
                                color="primary"
                                value={forms.screeningCVForm.submitFlag ? 50 : 0}                        
                                 />
                                 <span>{forms.screeningCVForm.submitFlag ? "50% Complete" : "0% Complete" }</span> 
                            </div>

                            <div className="screeningaprogress">
                                <h2>Academics</h2>
                                <ColorCircularProgress
                                variant="determinate"
                                color="primary"
                                value={50}                        
                                 />
                                 <span>50% Complete</span>
                            </div>

                            <div className="screeningaprogress">
                                <h2>Experience</h2>
                                <ColorCircularProgress
                                variant="determinate"
                                color="primary"
                                value={50}                        
                                 />
                                 <span>50% Complete</span>
                            </div>

                            <div className="screeningaprogress">
                                <h2>Award & Honors</h2>
                            </div>

                            <div className="screeningaprogress">
                                <h2>Languages</h2>
                            </div>
                        </div>
                    </Grid>
                </Grid> */}
            </div>
        )
    }
}

export default CandidateScreeningAContacDetail;