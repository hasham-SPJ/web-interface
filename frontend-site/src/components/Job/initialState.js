export const CANDIDATE = {
    data: {
        candidateFlowData: [
            {
                title: "Candidate",
                topText: "Screening A",
                bottomText: "VIDEO",
            },
            {
                title: "VISITORS",
                topText: "Hard Skills Test",
                bottomText: "Hackeron",
            },
            {
                title: "Candidate",
                topText: "Soft Skilss Test",
                bottomText: "EMOTIONAL intelligencs Test",
            },
            {
                title: "Candidate",
                topText: "Dashboard",
                bottomText: "ATTITUDE TEST",
            },
            {
                title: "Candidate",
                topText: "Dashboard",
                bottomText: "CHARTS with Applicant so far",
            }
        ],
        stepVal: 1,
        tests: {
            screening: {
                test: {
                    questions:[
                        {
                            id: 1,
                            content: 'When you\'re thinking to yourself, you tend to use positive words and encouraging phrases',
                            answers: [
                              {
                                content: 'True'
                              },
                              {
                                content: 'False'
                              }
                            ]
                        },
                    ],
                    currentQuestion: 0,
                    score: 0,
                    status: "pending",
                    readingStatus: "pending",
                    englishStatus: "pending",
                    state: 1
                }
            },
            hardskilss: {
                test: {
                    questions:[
                        {
                            id: 1,
                            content: 'When you\'re thinking to yourself, you tend to use positive words and encouraging phrases',
                            answers: [
                              {
                                content: 'True'
                              },
                              {
                                content: 'False'
                              }
                            ]
                        },
                    ],
                    currentQuestion: 0,
                    score: 0,
                    status: "pending",
                    closehardskills: false,
                }
            },
            eittest: {
                test: {
                    questions:[],
                    currentQuestion: 0,
                    score: 0,
                    status: "inprogress",
                    closeeittest: false,
                    isStart: false,
                }
            },
            attitudetest: {
                test: {
                    questions:[],
                    currentQuestion: 0,
                    score: 0,
                    status: "inprogress",
                    closeattitudetest: false,
                }
            },
            capstoneproject: {
                test: {
                    questions:[],
                    currentQuestion: 0,
                    score: 0,
                    status: "pending",
                    closeattitudetest: false,
                }
            },
        }
    },
    forms: {
        screeningCVForm: {
            formEL: false,
            name: "screeningCVForm",
            submitFlag: false,
            saveFlag: "unSaved",
            fields: {
                firstNameField: {
                    name: "firstNameField",
                    value: ""
                },
                lastNameField: {
                    name: "lastNameField",
                    value: ""
                },
                fatherNameField: {
                    name: "fatherNameField",
                    value: ""
                },
                cnicField: {
                    name: "cnicField",
                    value: ""
                },
                genderField: {
                    name: "genderField",
                    value: ""
                },
                maritalStatusField: {
                    name: "maritalStatusField",
                    value: ""
                },
                dateTimeField: {
                    name: "dateTimeField",
                    value: ""
                },
                emailField: {
                    name: "emailField",
                    value: "",
                },
                mobileNumberField: {
                    name: "mobileNumberField",
                    value: "",
                },
                addressField: {
                    name: "addressField",
                    value: "",
                },
                professionalSummaryField: {
                    name: "professionalSummaryField",
                    value: "",
                }
            }
        }
    },
    listData: {
    }
}

export default CANDIDATE;