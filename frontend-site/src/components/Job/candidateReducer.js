import {
    SET_STEP_VALUE,
    SET_SCREENING_STATUS_INPROGRESS_HANDLER,
    SET_DIALOG_HARDSKILSS_DIALOG_HANDLER,
    SET_DIALOG_EITTEST_DIALOG_HANDLER,
    UPDATE_SCREENING_CURRENT_QUESTION_HANDLER,
    UPDATE_SCREENING_SCORE_HANDLER,
    SET_HARDSKILSS_STATUS_INPROGRESS_HANDLER,
    UPDATE_HARDSKILLS_CURRENT_QUESTION_HANDLER,
    SET_EITTEST_STATUS_INPROGRESS_HANDLER,
    UPDATE_EITTEST_CURRENT_QUESTION_HANDLER,
    CANDIDATE_CV_SUBMISSION_TEXT_CHANGE_HaNDLER,
    CANDIDATE_CV_SUBMISSION_SELECT_CHANGE_HaNDLER,
    CANDIDATE_GET_ATTITUDE_TEST_DATA,
    CANDIDATE_GET_EIT_TEST_DATA,
    SCREENING_CHANGE_SUBMIT_FLAG_HANDLER,
    SCREENING_CHANGE_SAVE_FLAG_HANDLER,
    UPDATE_ATTITUDE_TEST_STATUS_HANDLER,
    UPDATE_EITTEST_START_STATUS_HANDLER,
    SET_CAPSTONE_PROJECT_STATUS_INPROGRESS_HANDLER,
    VIDEO_SCREENING_STATE_HANDLER,
    SET_SCREENING_READING_STATUS_INPROGRESS_HANDLER,
    SET_SCREENING_LISTENING_STATUS_INPROGRESS_HANDLER
} from "./actions";

export const candidateReducer = (state = [], action) => {
    switch (action.type) {
        case SET_STEP_VALUE:{
            return {
                ...state,
                data:{
                    ...state.data,
                    stepVal: action.payload
                }
            }
        }
        case SET_SCREENING_STATUS_INPROGRESS_HANDLER:{
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        screening: {
                            ...state.data.tests.screening,
                            test: {
                                ...state.data.tests.screening.test,
                                status: action.payload
                            }
                        }
                    }
                }
            }
        }

        case SET_SCREENING_READING_STATUS_INPROGRESS_HANDLER:{
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        screening: {
                            ...state.data.tests.screening,
                            test: {
                                ...state.data.tests.screening.test,
                                readingStatus: action.payload
                            }
                        }
                    }
                }
            }
        }

        case SET_SCREENING_LISTENING_STATUS_INPROGRESS_HANDLER:{
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        screening: {
                            ...state.data.tests.screening,
                            test: {
                                ...state.data.tests.screening.test,
                                englishStatus: action.payload
                            }
                        }
                    }
                }
            }
        }

        //hardskills dialog handler
        case SET_DIALOG_HARDSKILSS_DIALOG_HANDLER:{
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        hardskilss: {
                            ...state.data.tests.hardskilss,
                            test: {
                                ...state.data.tests.hardskilss.test,
                                closehardskills: action.payload
                            }
                        }
                    }
                }
            }
        }

        case SET_DIALOG_EITTEST_DIALOG_HANDLER:{
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        eittest: {
                            ...state.data.tests.eittest,
                            test: {
                                ...state.data.tests.eittest.test,
                                closeeittest: action.payload
                            }
                        }
                    }
                }
            }
        }

        //screening
        case UPDATE_SCREENING_CURRENT_QUESTION_HANDLER : {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        screening: {
                            ...state.data.tests.screening,
                            test: {
                                ...state.data.tests.screening.test,
                                currentQuestion: action.payload
                            }
                        }
                    }
                }
            }
        }
        case UPDATE_SCREENING_SCORE_HANDLER: {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        screening: {
                            ...state.data.tests.screening,
                            test: {
                                ...state.data.tests.screening.test,
                                score: action.payload
                            }
                        }
                    }
                }
            }
        }

        // hardskills
        case SET_HARDSKILSS_STATUS_INPROGRESS_HANDLER: {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        hardskilss: {
                            ...state.data.tests.hardskilss,
                            test: {
                                ...state.data.tests.hardskilss.test,
                                status: action.payload
                            }
                        }
                    }
                }
            }
        }
        case UPDATE_HARDSKILLS_CURRENT_QUESTION_HANDLER: {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        hardskilss: {
                            ...state.data.tests.hardskilss,
                            test: {
                                ...state.data.tests.hardskilss.test,
                                currentQuestion: action.payload
                            }
                        }
                    }
                }
            }
        }
        case SET_EITTEST_STATUS_INPROGRESS_HANDLER: {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        eittest: {
                            ...state.data.tests.eittest,
                            test: {
                                ...state.data.tests.eittest.test,
                                status: action.payload
                            }
                        }
                    }
                }
            }
        }
        case UPDATE_EITTEST_CURRENT_QUESTION_HANDLER: {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        eittest: {
                            ...state.data.tests.eittest,
                            test: {
                                ...state.data.tests.eittest.test,
                                currentQuestion: action.payload
                            }
                        }
                    }
                }
            }
        }

        case CANDIDATE_CV_SUBMISSION_TEXT_CHANGE_HaNDLER: {
            return {
                ...state,
                forms: {
                    ...state.forms,
                    screeningCVForm: {
                        ...state.forms.screeningCVForm,
                        fields: {
                            ...state.forms.screeningCVForm.fields,
                            [action.key]: {
                                ...state.forms.screeningCVForm.fields[action.key],
                                value: action.payload
                            }
                        }
                    }
                }
            }
        }

        case CANDIDATE_CV_SUBMISSION_SELECT_CHANGE_HaNDLER: {
            return {
                ...state,
                forms: {
                    ...state.forms,
                    screeningCVForm: {
                        ...state.forms.screeningCVForm,
                        fields: {
                            ...state.forms.screeningCVForm.fields,
                            [action.key]: {
                                ...state.forms.screeningCVForm.fields[action.key],
                                value: (action.payload === null) ? "" : action.payload
                            }
                        }
                    }
                }
            }
        }
        case CANDIDATE_GET_ATTITUDE_TEST_DATA: {
            return {
                ...state,
                data: {
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        attitudetest: {
                            ...state.data.tests.attitudetest,
                            test: {
                                ...state.data.tests.attitudetest.test,
                                questions: action.payload
                            }
                        }
                    }
                }
            }
        }
        case CANDIDATE_GET_EIT_TEST_DATA: {
            return {
                ...state,
                data: {
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        eittest: {
                            ...state.data.tests.eittest,
                            test: {
                                ...state.data.tests.eittest.test,
                                questions: action.payload
                            }
                        }
                    }
                }
            }
        }
        case SCREENING_CHANGE_SUBMIT_FLAG_HANDLER: {
            return {
                ...state,
                forms: {
                    ...state.forms,
                    screeningCVForm: {
                        ...state.forms.screeningCVForm,
                        submitFlag: action.payload,
                    }
                }
            }
        }
        case SCREENING_CHANGE_SAVE_FLAG_HANDLER: {
            return {
                ...state,
                forms: {
                    ...state.forms,
                    screeningCVForm: {
                        ...state.forms.screeningCVForm,
                        saveFlag: action.payload,
                    }
                }
            }
        }
        case UPDATE_ATTITUDE_TEST_STATUS_HANDLER: {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        attitudetest: {
                            ...state.data.tests.attitudetest,
                            test: {
                                ...state.data.tests.attitudetest.test,
                                status: action.payload
                            }
                        }
                    }
                }
            }
        }
        case UPDATE_EITTEST_START_STATUS_HANDLER: {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        eittest: {
                            ...state.data.tests.eittest,
                            test: {
                                ...state.data.tests.eittest.test,
                                isStart: action.payload
                            }
                        }
                    }
                }
            }
        }

        case SET_CAPSTONE_PROJECT_STATUS_INPROGRESS_HANDLER:{
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        capstoneproject: {
                            ...state.data.tests.capstoneproject,
                            test: {
                                ...state.data.tests.capstoneproject.test,
                                status: action.payload
                            }
                        }
                    }
                }
            }
        }

        case VIDEO_SCREENING_STATE_HANDLER: {
            return {
                ...state,
                data:{
                    ...state.data,
                    tests: {
                        ...state.data.tests,
                        screening: {
                            ...state.data.tests.screening,
                            test: {
                                ...state.data.tests.screening.test,
                                state: action.payload
                            }
                        }
                    }
                }
            }
        }
        default: {
            return {
                ...state
            };
        }
    }
}