import { connect } from "react-redux";
import instance, { BASE_URL } from "../../../api/config";
import CandidateCVSubmission from "./component";
import {
    textFieldChageHandlerScreening,
    selectFieldChageHandlerScreening,
    screeningSubmitFlagHandler,
    screeningSaveStatusHandler
} from "../actions";

const mapStateToProps = state => ({
    fields: state.CANDIDATE.forms.screeningCVForm.fields,
    forms: state.CANDIDATE.forms,
})

const mapDispatchToProps = (dispatch, props) => ({
    textFieldChageHandler: (name, e) => {
        if (typeof e.target.value !== "string") {
            e.target.value = "";
        }
        if (typeof e.target.value === null) {
            e.target.value = "";
        }
        dispatch(textFieldChageHandlerScreening(name, e.target.value));
    },
    selectFieldChageHandler: (name, e) => {
        dispatch(selectFieldChageHandlerScreening(name, e.target.value));
    },
    submitScreeningCV: (value) => {
        dispatch(screeningSaveStatusHandler("saving"));
         instance.post(BASE_URL+'SubmitCV', value)
        .then(res => {
            dispatch(screeningSubmitFlagHandler());
            dispatch(screeningSaveStatusHandler("saved"));
        })
        .catch(error => {
            
        })
    }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    submitScreeningCV: () => {
        const value = {
            firstName: "a",
            lastName: "a",
            designation: "null",
            description: "string",
            email: "test@gmail.com",
            cellNo: "null",
            phoneNo: "123",
            country: "Pakistan",
            company: "string",
            city: "null",
            jobTitle: "string",
            from: "2020-03-30T08:07:23.252Z",
            to: "2020-03-30T08:07:23.252Z",
            current: true,
            institute: "string",
            degree: "string",
            instituteCity: "string",
            studiedFrom: "2020-03-30T08:07:23.252Z",
            studiedTo: "2020-03-30T08:07:23.252Z",
            skills: "string",
            certifications: "string"
          }
        dispatchProps.submitScreeningCV(value);
    },
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(CandidateCVSubmission);