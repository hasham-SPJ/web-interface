import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import ColorCircularProgress from '@material-ui/core/LinearProgress';
import CVMainImg from "../../../cvsideimg.png";
import KeyboardBackSpace from "@material-ui/icons/KeyboardBackspace";

class CandidateCVSubmission extends Component {
    constructor(props) {
        super(props);
        this.state = {
            basicInformation: true,
            contactInformation: false,
            employmentHistory: false,
            educationHistory: false,
            relevantSkills: false,
            certification: false,
            completed: false,
            progresValue: 0,
        }
    }

    progressValue = (value) => {
        this.setState({
            progresValue: value
        })
    }

    basicInformationHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: true,
        })
        this.progressValue(10);
    }
    contactInformationHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: false,
            employmentHistory: true,
        })
        this.progressValue(20);
    }
    emplomentHistoryHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: false,
            employmentHistory: false,
            educationHistory: true,
        })
        this.progressValue(40);
    }
    educationHistoryHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: false,
            employmentHistory: false,
            educationHistory: false,
            relevantSkills: true,
        })
        this.progressValue(70);
    }
    relevanSkillsHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: false,
            employmentHistory: false,
            educationHistory: false,
            relevantSkills: false,
            certification: true,
        })
        this.progressValue(100);
    }
    submitHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: false,
            employmentHistory: false,
            educationHistory: false,
            relevantSkills: false,
            certification: false,
            completed: true,
        })
        this.props.submitScreeningCV();
    }
    contactToBasicInformationHandler = () => {
        this.setState({
            basicInformation: true,
            contactInformation: false,
            employmentHistory: false,
        })
    }
    employToContactInformationHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: true,
            employmentHistory: false,
        })
    }
    educationToEmployInformationHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: false,
            employmentHistory: true,
            educationHistory: false,
        })
    }
    relevantToEducationInformationHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: false,
            employmentHistory: false,
            educationHistory: true,
            relevantSkills: false,
        })
    }
    certificationToRelevantInformationHandler = () => {
        this.setState({
            basicInformation: false,
            contactInformation: false,
            employmentHistory: false,
            educationHistory: false,
            relevantSkills: true,
            certification: false,
        })
    }
    render() {
        const {
            fields,
            textFieldChageHandler,
            selectFieldChageHandler,
            // forms
        } = this.props;
        
        return (
            <div className="candidateCVWrapper">
                <div className="buildcvmaintitle" >
                    <h1>Building your CV</h1>
                    <ColorCircularProgress
                        variant="determinate"
                        color="primary"
                        value={this.state.progresValue}
                        className="candidatecvprogress"                    
                            />
                </div>
                <p>Please fill relevant information and we will build your CV for you.</p>
                <Grid container className="maincvsubmissionfields" justify="center">
                    <Grid item sm={12} md={5}>
                       {
                           this.state.basicInformation ? 
                        <>
                        <h4>Basic Information</h4>
                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="First Name"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Last Name"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Designation"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className={` maincvtextfields  maincvtextareafields`}
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder = "Short Description"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                multiline
                                rows="4"
                                />
                                <Button onClick={this.basicInformationHandler} variant="contained" color="primary">
                                    Next
                                </Button>
                            </> : null }




                            
                            {/* contact information start here  */}
                            {
                           this.state.contactInformation ? 
                        <>
                        <h4> <Button onClick={this.contactToBasicInformationHandler} variant="outlined" color="primary">
                                    <KeyboardBackSpace />
                                </Button>
                                Contact Information</h4>
                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Email"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Cell"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Phone"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder = "Address"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder = "Country"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />
                                <Button onClick={this.contactInformationHandler} variant="contained" color="primary">
                                    Next
                                </Button>
                            </> : null }



                            {/* employement history start here  */}

                            {
                           this.state.employmentHistory ? 
                        <>
                        <h4> <Button onClick={this.employToContactInformationHandler} variant="outlined" color="primary">
                                    <KeyboardBackSpace />
                                </Button>
                                Employment history</h4>
                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Company"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Designation"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Salary"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder = "From"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder = "To"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                            <Button variant="contained" color="primary" className="cvaddanotherbtn">
                                Add Another
                            </Button>
                                <Button onClick={this.emplomentHistoryHandler} variant="contained" color="primary">
                                    Next
                                </Button>
                            </> : null }



                            {/* education history starts here  */}


                            {
                           this.state.educationHistory ? 
                        <>
                        <h4> <Button onClick={this.educationToEmployInformationHandler} variant="outlined" color="primary">
                                    <KeyboardBackSpace />
                                </Button>
                                Education history</h4>
                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Institute"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Degree"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="City"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder = "From"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder = "To"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                            <Button variant="contained" color="primary" className="cvaddanotherbtn">
                                Add Another
                            </Button>
                                <Button onClick={this.educationHistoryHandler} variant="contained" color="primary">
                                    Next
                                </Button>
                            </> : null }



                            {/* relevant skills starts here  */}

                            {
                           this.state.relevantSkills ? 
                        <>
                        <h4> <Button onClick={this.relevantToEducationInformationHandler} variant="outlined" color="primary">
                                    <KeyboardBackSpace />
                                </Button>
                                Relevant skills</h4>
                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Add Skill"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Experience Level"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />
                            

                            <Button variant="contained" color="primary" className="cvaddanotherbtn">
                                Add Another
                            </Button>

                            <div className="skilsboxes">
                                <span>React Js</span>
                                <span>ASP.Net</span>
                                <span>PHP</span>
                                <span>Scrum</span>
                                <span>ios</span>
                            </div>

                                <Button onClick={this.relevanSkillsHandler} variant="contained" color="primary">
                                    Next
                                </Button>
                            </> : null }


                            {/* certifications starts here  */}


                            {
                           this.state.certification ? 
                        <>
                        <h4> <Button onClick={this.certificationToRelevantInformationHandler} variant="outlined" color="primary">
                                    <KeyboardBackSpace />
                                </Button>
                                Certifications</h4>
                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Certificate or Award"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />

                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Issuing Organization"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />
                        
                        <TextField
                                className="maincvtextfields"
                                // id={fields.addressField.name}
                                // name={fields.addressField.name}
                                // value={fields.addressField.value}
                                placeholder="Year"
                                // onChange={value => {
                                //     textFieldChageHandler(
                                //         fields.addressField.name,
                                //         value
                                //     )
                                // }}
                                />
                            

                            <Button variant="contained" color="primary" className="cvaddanotherbtn">
                                Add Another
                            </Button>

                                <Button onClick={this.submitHandler} variant="contained" color="primary">
                                    Submit
                                </Button>
                            </> : null }
                            

                            

                    </Grid>
                    <Grid item sm={12} md={4}>
                        {
                            this.state.completed ? null :
                        <img src={CVMainImg} className="cvbuildingsideimg" ></img>
                        }
                            
                    </Grid>

                    {
                        this.state.completed ? 
                        <Grid item sm={12}>
                            <div className="finalcvsubmitted">
                                <h1>CONGRATULATIONS!</h1>
                                <p>You are already on your way to start your career</p>
                                <Button href="/" variant="contained" color="primary">
                                    View Profile
                                </Button>
                            </div>
                        </Grid>
                        : null
                    }
                    
                </Grid>
            </div>
        )
    }
}

export default CandidateCVSubmission;