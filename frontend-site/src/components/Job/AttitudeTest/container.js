import { connect } from "react-redux";
import CandidateAttitudeTest from "./component";
import {
    attitudeTestStatusHandler,
    eitStartStatusHandler
} from "../actions";

const mapStateToProps = state => ({
    attitudetest: state.CANDIDATE.data.tests.attitudetest,
})

const mapDispatchToProps = (dispatch, props) => ({
    attitudeTestStatusHandler: () => {
        dispatch(attitudeTestStatusHandler("completed"));
    },
    eitStartStatusHandler: () => {
        dispatch(eitStartStatusHandler());
    }
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(CandidateAttitudeTest);