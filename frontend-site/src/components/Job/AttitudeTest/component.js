import React, { Component } from "react";
import "./style.css";
import { Button, Grid } from '@material-ui/core';
import TestTemplate from "../QuestionBox/container";
import TestScore from "../TestScore/container";
import Timer from "react-compound-timer";
import QueryBuilderIcon from "@material-ui/icons/QueryBuilder";

class CandidateAttitudeTest extends Component {
    render() {
        const {
            attitudetest,
            attitudeTestStatusHandler,
            eitStartStatusHandler
        } = this.props;
        return (
            
            <div className="attitudetestwrapper">
                {attitudetest.test.status === "pending" ? 
                <>
                <h3>Attitude Test</h3>
                <h4>Duration: 10 Mins</h4>
                <Button variant="contained" color="primary" className="starttestbtn">
                    Start Test
                </Button>
                <p>
                    <b>Scope: </b> <br></br>
                    The resolve to develop this application module is to build a test system where the candidate can take a soft skill test, soft skill test is vital to know the candidate is able to collaborate, contribute, meet deadlines, and understands the end user's perspective. The assessment will be a series of questions of different types which are pulled randomly from a test bank.
                </p>
                </> : null }

                {
                    
                    attitudetest.test.status === "inprogress" ? 
                    <>
                    
                    <TestTemplate title="Attitude Test" testDetail={attitudetest}  submitHandler={attitudeTestStatusHandler} />
                    </>
                     : null
                }

                {
                    attitudetest.test.status === "completed" ? 
                    <>
                        <TestScore showButton={true} score={attitudetest.test.score} nextSubmitHandler={eitStartStatusHandler}/>
                    </>
                    : null
                }
            </div>
            
        )
    }
}

export default CandidateAttitudeTest;