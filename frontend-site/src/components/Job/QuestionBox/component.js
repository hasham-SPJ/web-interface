import React, {Component} from 'react';
import "./style.css"
import { Button, Grid } from '@material-ui/core';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Timer from "react-compound-timer";
import QueryBuilderIcon from "@material-ui/icons/QueryBuilder";

class QuestionBox extends Component {
    render() {
        const {
            submitHandler,
            testDetail,
            title
        } = this.props;
        const currentQuestion = testDetail.test.questions;
        // const currentQuestionAnswer = testDetail.test.questions[testDetail.test.currentQuestion].answers;
        return (
            <>
                <Grid item sm={12} className="questionmaintestboxtitle">
                <h2 >JOBAi/<span>Candidate Flow/{title}</span></h2>
                    <Timer
                            initialTime={600000}
                            direction="backward"
                            checkpoints={[
                                {
                                    time: 0,
                                    callback: () => submitHandler(),
                                }
                            ]}
                        >
                            {() => (
                                <React.Fragment>
                                    <h2 class="timer"> 
                                        <QueryBuilderIcon />
                                        Time Left: 
                                        <Timer.Minutes /> <span>: </span>
                                        <Timer.Seconds /> <span></span>
                                    </h2>
                                </React.Fragment>
                            )}
                        </Timer>
                </Grid>
                
                 <div className="questionbox">
                 
                    <div className="questioboxwrapper">
                    {
                        (currentQuestion) ? 
                    
                    <>
                    {
                        currentQuestion.map((questions, index) => (
                            <>
                            <h3>{index + 1} . {questions.content}</h3>
                            
                        
                    

                    <Grid container>
                        <Grid item xs={12} sm={12} md={12}>
                        <FormControl component="fieldset">
                        <RadioGroup aria-label="gender" name="gender1" className="testradiogroup">
                            {
                                questions.answers.map(test => (
                                    <>
                                        <FormControlLabel value={test.content} control={<Radio />} label={test.content}
                                        //  onClick={(e) => saveRightAnswer(currentQuestion.rightAnswer, test.answer, testDetail.test.score)}
                                         />
                                    </>
                                ))
                            }
                         </RadioGroup>
                        </FormControl> 
                         
                         </Grid>

                        {/* <Grid item xs={12} sm={12} md={12}> 
                         <Button variant="contained" color="primary">
                            <KeyboardArrowLeftIcon />
                        </Button> 
                         <Button variant="contained" color="primary" disabled={(testDetail.test.questions.length - 1) === testDetail.test.currentQuestion ? true : false} onClick={updateScreeningCurrentQuestionCounter}>
                            <NavigateNextIcon /> Next
                        </Button>
                        </Grid> */}
                    </Grid> 
                    </>
                    ))
                    }
                    <Button onClick={submitHandler} variant="contained" color="primary" className="submitbtn">
                        Save Ans
                    </Button> 
                </> : <h1>Loading...</h1>}
                    
                     </div>
                    {/* <Button onClick={submitHadnler} variant="contained" disabled={(testDetail.test.questions.length - 1) === testDetail.test.currentQuestion ? false : true} color="primary" className="submitquizbtn" >
                        Submit Quiz
                    </Button> */}
                </div> 
            </>
        )
    }
}

export default QuestionBox;





