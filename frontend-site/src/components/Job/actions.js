import instance, { BASE_URL } from "../../api/config";

export const SET_STEP_VALUE = "SET_STEP_VALUE";
export const SET_SCREENING_STATUS_INPROGRESS_HANDLER = "SET_SCREENING_STATUS_INPROGRESS_HANDLER";
export const SET_DIALOG_HARDSKILSS_DIALOG_HANDLER = "SET_DIALOG_HARDSKILSS_DIALOG_HANDLER";
export const SET_DIALOG_EITTEST_DIALOG_HANDLER = "SET_DIALOG_EITTEST_DIALOG_HANDLER";
export const UPDATE_SCREENING_CURRENT_QUESTION_HANDLER = "UPDATE_SCREENING_CURRENT_QUESTION_HANDLER";
export const UPDATE_SCREENING_SCORE_HANDLER = "UPDATE_SCREENING_SCORE_HANDLER";

export const SET_HARDSKILSS_STATUS_INPROGRESS_HANDLER = "SET_HARDSKILSS_STATUS_INPROGRESS_HANDLER";
export const UPDATE_HARDSKILLS_CURRENT_QUESTION_HANDLER = "UPDATE_HARDSKILLS_CURRENT_QUESTION_HANDLER";

export const SET_EITTEST_STATUS_INPROGRESS_HANDLER = "SET_EITTEST_STATUS_INPROGRESS_HANDLER";
export const UPDATE_EITTEST_CURRENT_QUESTION_HANDLER = "UPDATE_EITTEST_CURRENT_QUESTION_HANDLER";
export const UPDATE_EITTEST_START_STATUS_HANDLER = "UPDATE_EITTEST_START_STATUS_HANDLER";

export const CANDIDATE_CV_SUBMISSION_TEXT_CHANGE_HaNDLER = "CANDIDATE_CV_SUBMISSION_TEXT_CHANGE_HaNDLER";
export const CANDIDATE_CV_SUBMISSION_SELECT_CHANGE_HaNDLER = "CANDIDATE_CV_SUBMISSION_SELECT_CHANGE_HaNDLER";

export const CANDIDATE_GET_ATTITUDE_TEST_DATA = "CANDIDATE_GET_ATTITUDE_TEST_DATA";
export const CANDIDATE_GET_EIT_TEST_DATA = "CANDIDATE_GET_EIT_TEST_DATA";
export const UPDATE_ATTITUDE_TEST_STATUS_HANDLER = "UPDATE_ATTITUDE_TEST_STATUS_HANDLER";

export const SCREENING_CHANGE_SUBMIT_FLAG_HANDLER = "SCREENING_CHANGE_SUBMIT_FLAG_HANDLER";
export const SCREENING_CHANGE_SAVE_FLAG_HANDLER = "SCREENING_CHANGE_SAVE_FLAG_HANDLER";

export const SET_CAPSTONE_PROJECT_STATUS_INPROGRESS_HANDLER = "SET_CAPSTONE_PROJECT_STATUS_INPROGRESS_HANDLER";
export const VIDEO_SCREENING_STATE_HANDLER = "VIDEO_SCREENING_STATE_HANDLER";

export const SET_SCREENING_READING_STATUS_INPROGRESS_HANDLER = "SET_SCREENING_READING_STATUS_INPROGRESS_HANDLER";
export const SET_SCREENING_LISTENING_STATUS_INPROGRESS_HANDLER = "SET_SCREENING_LISTENING_STATUS_INPROGRESS_HANDLER";

export const setStepVal = (value) => {
    return {
      type: SET_STEP_VALUE,
      payload: value
    };
  };

  export const screeningTestStatusHandler = (value) => {
    return {
      type: SET_SCREENING_STATUS_INPROGRESS_HANDLER,
      payload: value,
    };
  };

  export const screeningReadingStatusHandler = (value) => {
    return {
      type: SET_SCREENING_READING_STATUS_INPROGRESS_HANDLER,
      payload: value
    }
  }

  export const screeningListeningStatusHandler = (value) => {
    return {
      type: SET_SCREENING_LISTENING_STATUS_INPROGRESS_HANDLER,
      payload: value
    }
  }

  export const capstoneProjectStatusHandler = (value) => {
    return {
      type: SET_CAPSTONE_PROJECT_STATUS_INPROGRESS_HANDLER,
      payload: value,
    }
  }
  export const closehardskillsdialogHandler = (value) => {
    return {
      type: SET_DIALOG_HARDSKILSS_DIALOG_HANDLER,
      payload: value,
    }
  }

  export const closeeittestdialogHandler = (value) => {
    return {
      type : SET_DIALOG_EITTEST_DIALOG_HANDLER,
      payload: value,
    }
  }

  //screening
  export const updateScreeningCurrentQuestionCounter = (value) => {
    return {
      type: UPDATE_SCREENING_CURRENT_QUESTION_HANDLER,
      payload: value,
    }
  }

  export const saveRightAnswer = (score) => {
    return {
      type: UPDATE_SCREENING_SCORE_HANDLER,
      payload: score,
    }
  }

  //harskilss
  export const hardskilssTestStatusHandler = (value) => {
    return {
      type: SET_HARDSKILSS_STATUS_INPROGRESS_HANDLER,
      payload: value,
    };
  }

  export const updateHardskillsCurrentQuestionCounter = (value) => {
    return {
      type: UPDATE_HARDSKILLS_CURRENT_QUESTION_HANDLER,
      payload: value,
    }
  }

  //eittest
  export const eitTestStatusHandler = (value) => {
    return {
      type: SET_EITTEST_STATUS_INPROGRESS_HANDLER,
      payload: value,
    };
  }
  export const updateeittestCurrentQuestionCounter = (value) => {
    return {
      type: UPDATE_EITTEST_CURRENT_QUESTION_HANDLER,
      payload: value,
    }
  }

  export const textFieldChageHandlerScreening = (name, value) => dispatch => {
    dispatch({
      type:CANDIDATE_CV_SUBMISSION_TEXT_CHANGE_HaNDLER,
      payload: value,
      key: name,
    })
  }

  export const selectFieldChageHandlerScreening = (name, value) => dispatch => {
    dispatch({
      type:CANDIDATE_CV_SUBMISSION_SELECT_CHANGE_HaNDLER,
      payload: value,
      key: name,
    })
  }


  export const submitScreeningCV = (value) => {
    return {
      
    }
    // instance.post('http://3.134.252.39:5005/api/SubmitCV', value)
    // .then(res => {

    // })
    // .catch(error => {
        
    // })
}
export const screeningSubmitFlagHandler = () => {
  return {
    type:SCREENING_CHANGE_SUBMIT_FLAG_HANDLER,
    payload: true,
  }
}

export const screeningSaveStatusHandler = (value) => {
  return {
    type: SCREENING_CHANGE_SAVE_FLAG_HANDLER,
    payload: value,
  }
}

export const getAttitudeTest = value => {
  return {
    type: CANDIDATE_GET_ATTITUDE_TEST_DATA,
    payload: value
  }
}
export const attitudeTestStatusHandler = value => {
  return {
    type: UPDATE_ATTITUDE_TEST_STATUS_HANDLER,
    payload: value,
  }
}

export const eitStartStatusHandler = () => {
  return {
    type: UPDATE_EITTEST_START_STATUS_HANDLER,
    payload: true,
  }
}

export const getEITTest = (value) => {
  return {
    type: CANDIDATE_GET_EIT_TEST_DATA,
    payload: value
  }
}

export const screeningStateHandler = (value) => {
  return {
    type: VIDEO_SCREENING_STATE_HANDLER,
    payload: value
  }
}