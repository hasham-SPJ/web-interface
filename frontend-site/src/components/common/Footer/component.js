import React, {Component} from 'react';
import "./style.css";
import { Button, TextField , Grid }  from '@material-ui/core';

class Footer extends Component {
    render() {

        return (
        <>
            <div className="footerwrapper">
                <Grid container justify="center">
                    <Grid item xs={12} sm={6} md={3}>
                        <div className="footersection">
                            <h1>Specialised Jobs</h1>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2}>
                        <div className="footersection">
                            <h2>About</h2>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Why Us</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Blogs</a></li>
                            </ul>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2}>
                        <div className="footersection">
                            <h2>Company</h2>
                            <ul>
                                <li><a href="#">Team</a></li>
                                <li><a href="#">For Employer</a></li>
                                <li><a href="#">For Professionals</a></li>
                                <li><a href="#">Trainings</a></li>
                                <li><a href="#">Job Ai</a></li>
                                <li><a href="#">Agora</a></li>
                            </ul>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2}>
                        <div className="footersection">
                            <h2>Contact</h2>
                            <ul>
                                <li><a href="#">Customer Helpline</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">FAQ</a></li>
                            </ul>
                        </div>
                    </Grid>
                </Grid>
                
            </div>
            <div className="footerbottom">
                <p>&copy; Specialised Jobs 2020.. All Rights Reserved.</p>
            </div>
        </>
        )
    }
}

export default Footer;