import React, { Component } from "react";
import { Typography, Card, CardContent, Button, Grid} from "@material-ui/core";
import inserticon from './AgoraAssets/img/inserticon.svg';
import viewicon from './AgoraAssets/img/ViewIcon.svg';
import viewstats from './AgoraAssets/img/ViewStats.svg';
import './AgoraLandingPage.css';


export default class AgoraHome extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
    }
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {

    return (
   
        <div className="MainDiv" id="rootagora">
        <div className="NavDiv"> 
          <Grid container className="MainGrid"   >  
          <Grid item xs className="LandingTab1" >
            <card>
              <Typography align="left" variant="h6" >
            Welcome To 
         </Typography>
              <Typography align="left" variant="h2" className="TextAgora"  >
            AGORA  
         </Typography>
         <Typography align="left" variant="subtitle1" className="Subheading" >
         You can access all things relevant to <br></br> marketing using this interface. 
         </Typography>
         <Button variant="contained" color="primary" target="_blank" href="https://docs.google.com/document/d/1BrnxI11ovQI0eMouOAMhn7Yn4cn8Mt3t/edit#" className="DownloadButton" >
            Download User Manual
          </Button>
          </card>
            </Grid>
            <Grid item xs >
              <a href="/agora/insertdata">
              <Card className="LandingTab2" >
                  <CardContent >
                  <img className="InsertImage" src={inserticon} alt="" />
                    <Typography align="center" variant="h6" component="h2" >
                      Insert  Data
                    </Typography> 
                  </CardContent>
              </Card>
              </a>
            </Grid>
            
            <Grid item xs >
            <a href="/agora/viewdata">
              <Card className="LandingTab2" >
                  <CardContent >
                  <img className="InsertImage" src={viewicon} alt="" />
                    <Typography align="center" variant="h6" component="h2" >
                      View Data
                    </Typography>
                  </CardContent>
              </Card>
              </a>
            </Grid>
            
            <Grid item xs >
            <a href="/agora/stats">
              <Card className="LandingTab2" >
                  <CardContent >
                  <img className="InsertImage" src={viewstats} alt=""/>
                    <Typography align="center" variant="h6" component="h2" >
                      View Statistics
                    </Typography>
                  </CardContent>
              </Card>
              </a>
            </Grid>
          </Grid>
         
           <div className="Bottom"></div>
      
      </div>
      </div>
    );
  };
}