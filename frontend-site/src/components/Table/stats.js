import React, { Component } from 'react';
import {Typography,InputLabel} from '@material-ui/core';
import { Chart } from "react-google-charts";
import './Stats.css';



export default class stats extends Component {
  constructor(props) {
    super(props);
    this.state = {
        selectedComponent: 1,
    }
}
DataChangeHandler = (event) => {
    this.setState({
        selectedComponent: event.target.value,
    });
}
  render() {

    return (
      <div className="stats">
     <div className="HeaderBar">
                    <ul  >
                        <li className="AgoraHeaderNav" >
                            <Typography align="left" variant="h4" className="TextAgora1"  >
                                AGORA 
                            </Typography>       
                        </li >
                        <span onClick={this.DataChangeHandler}>
                        <li className="NavHome" value={1}>
                        <InputLabel >Salaries</InputLabel> 
                        </li>
                        <li className="NavHome" value={2}>
                        <InputLabel >Location</InputLabel>
                        </li>
                   
                        </span>
  
                    </ul>
                </div>

          {this.state.selectedComponent === 1 ?
                    <>
         <Typography align="center" variant="h4" className="Salarytext"  >
         Average Salaries in Pakistan
         </Typography> 
     
         <div className="Chart">
      <Chart
            chartType="Bar"
            legendToggle
            spreadSheetUrl="https://docs.google.com/spreadsheets/d/1VqO8PgRa2hsYDc3f3es49s2Bqr_2vkIATraWQQKOa-c/edit#gid=0"
          
            options={{
              
              bars: 'vertical',
              colors: ['329FF2'],
              pointSize: 10,
              title: 'Average Salaries in Pakistan',
              width: "300%",
              height: 600,
              vAxis: {
                title: "Age",
                viewWindow: {
                  title: "Salary",
                  max: -10,
                  min: 100,
                },
              },
              hAxis: {
                viewWindow: {
                  max: 100,
                  min: -10,
                },
              },
              
            }}
          />
          </div>
          </> : null}

          {this.state.selectedComponent === 2 ?
                    <>
         <Typography align="center" variant="h4" className="Salarytext"  >
         IT Companies in Cities
         </Typography> 
     

      <Chart
            chartType="PieChart"
            data={[
              ['Cities', 'IT Companies'],
              ['Islamabad', 139],
              ['Rawalpindi', 22],
              ['Lahore', 200],
              ['Faisalabad', 9],
              ['Bahawlpur', 2],
              ['Hyderabad', 4],
              ['Karachi',275]
            ]}
            width={'100%'}
            height={'900px'}
            
            loader={<div>Loading Chart</div>}
            options={{

              is3D: true,
            }}
            rootProps={{ 'data-testid': '2' }}
          />

          </> : null}
          <div className="Bottom">
          </div>

      </div>
    );
  }
}
