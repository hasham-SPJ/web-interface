import React, { Component } from "react";
import { Typography, Card, CardContent, Button, Grid} from "@material-ui/core";
import inserticon from './AgoraAssets/img/inserticon.svg';
import viewicon from './AgoraAssets/img/ViewIcon.svg';
import viewstats from './AgoraAssets/img/ViewStats.svg';
import './AgoraLandingPage.css';


export default class ViewDashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
    }
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {

    return (
   
        <div className="MainDiv">
        <div className="NavDiv"> 
          <Grid container className="MainGrid"   >  
          <Grid item xs className="LandingTab1" >
            <card>
              <Typography align="left" variant="h6" >
            Welcome To 
         </Typography>
              <Typography align="left" variant="h3" className="TextAgora"  >
            AGORA DATA CENTER 
         </Typography>
         <Typography align="left" variant="subtitle1" className="Subheading" >
         You can view marketing data or Search data  <br></br> Click on view data to proceed
         </Typography>
       
          </card>
            </Grid>
            <Grid item xs >
            <a href="/agora/companydata">
              <Card className="LandingTab2" >
                  <CardContent >
                  <img className="InsertImage" src={viewicon} />
                    <Typography align="center" variant="h6" component="h2" >
                      View Data
                    </Typography>
                  </CardContent>
              </Card>
              </a>
            </Grid>
            <Grid item xs >
              <div className="LandingTab3" >
                    <Typography align="center" variant="h6" component="h2" >
                      Total Companies
                    </Typography> 
                    <Typography align="left" variant="h3" className="TextAgora3"  >
            384
         </Typography>
         </div>
    
            </Grid>
            
        
            
            <Grid item xs >
            <div className="LandingTab3" >
                 
                    <Typography align="center" variant="h6" component="h2" >
                      Total Cities
                    </Typography>
                    <Typography align="left" variant="h3" className="TextAgora3"  >
            7
         </Typography>

              </div>
            </Grid>
          </Grid>
         
           <div className="Bottom"></div>
      
      </div>
      </div>
    );
  };
}