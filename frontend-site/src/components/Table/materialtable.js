import React, { Component } from "react";
import { Chart } from "react-google-charts";
import { Typography ,Button,MenuItem,Select,InputLabel,FormControl } from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import './AgoraLandingPage.css';



export default class AgoraHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedComponent: 1,
        }
    }
    DataChangeHandler = (event) => {
        // console.log('e', event);
        this.setState({
            selectedComponent: event.target.value,
        });
    }

    render() {


        return (

            <div >
                      {/* Navigation Bar */}

                <div className="HeaderBar">
                    <ul >
                        <li className="AgoraHeaderNav" >
                            <Typography align="left" variant="h4" className="TextAgora1"  >
                                AGORA 
                            </Typography>       
                        </li>
                        <li className="AgoraHeaderNav">
                            <FormControl variant="outlined" className="SearchOptions">
                                <InputLabel  >Data Source</InputLabel>
                                <Select onChange={this.DataChangeHandler} className="SourceDropdown">
                                    <MenuItem value={1} >GOODFIRM</MenuItem>
                                    <MenuItem value={2} >P@SHA</MenuItem>
                                    <MenuItem value={3} >Other</MenuItem>
                                </Select>
                            </FormControl>
                        </li>
                        <li>
                            <Button
                             className="SheetButton" 
                             variant="contained"
                             color="primary" 
                             target="_blank"
                             href="https://drive.google.com/drive/folders/14XvJxaMJXvCevJl3X_qddvQ8i1dG5zPl?usp=sharing"  >                       View Google Sheet
                            </Button>
                        </li>
                        <li>
                        <SearchIcon className="Searchicontop" fontSize="large"/>
                        </li>
                        
                    </ul>
                </div>


                  {/* Tables */}

                {this.state.selectedComponent === 1 ?
                    <>
                        <Chart
                            chartType="Table"
                            loader={<div>Loading Data</div>}
                            spreadSheetUrl="https://docs.google.com/spreadsheets/d/1LTauofCzGlNiXmkvgs3FWmAx6Z62f0W7QOi5GeAFBZw/edit#gid=0"
                            options={{

                                width: "100%",
                                
                                // height: 800,
                                // bar: { groupWidth: '95%' },
                                // legend: { position: 'none' },
                                     }}
                            rootProps={{ 'data-testid': '1' }}
                            controls={[
                                {
                                    controlType: 'StringFilter',
                                    options: {
                                        filterColumnIndex: 6,
                                        matchType: 'any', // 'prefix' | 'exact',
                                        label: 'Search by City',
                                        tooltip: { focusTarget: 'Focus area',isHtml: true, trigger: "visible" }

                                    },
                                },
                            ]}
                        />
                    </> : null}

                {this.state.selectedComponent === 2 ?
                    <Chart
                        chartType="Table"
                        loader={<div>Loading Chart</div>}
                        spreadSheetUrl="https://docs.google.com/spreadsheets/d/1Kn7nQMUeyLT_9N-0l4i393IzsdpPGK_S_7QuXv0GKAE/edit#gid=0"
                        options={{

                            width: "100%",
                            // height: 800,
                            // bar: { groupWidth: '95%' },
                            // legend: { position: 'none' },
                        }}
                        rootProps={{ 'data-testid': '1' }}
                        controls={[
                            {
                                controlType: 'StringFilter',
                                options: {
                                    filterColumnIndex: 5,
                                    matchType: 'any', // 'prefix' | 'exact',

                                },
                            },
                        ]}
                    />: null}
            </div>
        );
    };
}
