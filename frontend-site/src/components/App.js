import React, { Component } from 'react';
import {BrowserRouter as Router, Switch,Route, Redirect} from "react-router-dom";
import Dashboard from "./Dashboard/component";
import JobAI from "./Job/container";
import AGORA from "./Agora/component";
import TestPage from "./Test/component";
import EITTestPage from "./Test/EIT/component";
import ResultPage from  "./Test/Result/component";
import CompanyData from "./Table/materialtable";
import InsertData from "./Table/InsertData";
import StatsData from "./Table/stats";
import AgoraLandingPage from "./Table/agoralandingpage";
import JobAILandingPage from "./Job/Landingpage/container";
import JobAISelectCategory from "./Job/SelectCatagory/container";
import JobAIStartTest from "./Job/StartTest/container";
import JobAIPerformanceDashboard from "./Job/PerformanceDashboard/container";
import CompanyDatabase from "./Table/ViewDataMain"
import JobAICorporateDashboard from "./Job/Corporate/Dashboard/container";
import JobAICorporateAssignProject from "./Job/Corporate/Projects/Assign/container";
import JobAICorporateViewProject from "./Job/Corporate/Projects/View/container";
import JobAICorporateEvaluatedProject from "./Job/Corporate/Projects/Evaluated/container";
import CandidateScreeningAProfile from "./Job/ScreeningA/Profile/container";
import JobAICorporatePassedProject from "./Job/Corporate/Projects/Passed/container";

import "./style.css";
class App extends Component {
  render() {
    return (
      <>
        <Dashboard />
        <div className="App">
          <Router>
                <Switch>
                    <Route exact path={`/`} component={JobAILandingPage}/>
                    <Route exact path={`/jobai`} component={JobAILandingPage}/>
                    <Route path={`/jobai/dashboard`} component={JobAIPerformanceDashboard}/>
                    <Route path={`/jobai/candidate`} component={JobAIStartTest}/>
                    <Route path={`/jobai/selectcategory`} component={JobAISelectCategory}/>
                    <Route path={`/jobai/candidate-test`} component={JobAI}/>
                    <Route path={`/jobai/candiate-profile`} component={CandidateScreeningAProfile} />
                    <Route path={`/jobai/corporate`} component={JobAICorporateDashboard}/>
                    <Route path={`/jobai/corporate-assign-project`} component={JobAICorporateAssignProject} />
                    <Route path={`/jobai/corporate-view-project`} component={JobAICorporateViewProject} />
                    <Route path={`/jobai/corporate-evaluated-project`} component={JobAICorporateEvaluatedProject} />
                    <Route path={`/jobai/corporate-passed-project`} component={JobAICorporatePassedProject} />
                    <Route exact path={`/agora`} component={AgoraLandingPage}/>
                     <Route path={`/agora/companydata`} component={CompanyData}/>
                    <Route path={`/agora/insertdata`} component={InsertData}/>
                     <Route path={`/agora/viewdata`} component={CompanyDatabase}/>
                    <Route exact path={`/agora/stats`} component={StatsData}/>
                    <Route exact path={`/test`} component={TestPage}/>
                    <Route exact path={`/test/eit`} component={EITTestPage}/>
                    <Route exact path={`/result`} component={ResultPage}/>
                </Switch>
          </Router>
        </div>
      </>
    );
  }
}

export default App;
